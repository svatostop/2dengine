NAME := engine1.exe
INCLUDES := \source\includes
SRCDIR := source\src
SRC  := main.cpp SpritesMainGAction.cpp ReadFile.cpp ParseParams.cpp Bitmap.cpp \
 Background.cpp Sprite.cpp Player.cpp TextHandler.cpp GameStartFirstLvl.cpp KeyHandle.cpp \
 KeyHandleSecView.cpp GameStartSecLvl.cpp Builder.cpp GameIntro.cpp

FLAGS := -s -lcomctl32 -Wl,--subsystem,windows -mwindows -mconsole  -lgdi32 -luser32 -lmsimg32 -lkernel32

RES := source\res\resource.rc
RESOBJ := source\res\resource.o

SRC := $(addprefix source\src\, $(SRC))

OBJS :=	$(SRC:.cpp=.obj)

all: $(NAME)
$(NAME): $(OBJS)
	windres -i $(RES) -o $(RESOBJ)
	g++ -g -I$(INCLUDES) $(OBJS) $(RESOBJ) -o $@ $(FLAGS)
	
%.obj : %.cpp
	g++ -c -o $@ $< 

clean:
	del /Q /F $(OBJS) engine1.exe

re: clean all