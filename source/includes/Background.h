#ifndef BACKGROUND_H
#define BACKGROUND_H

#include <windows.h>
#include "Bitmap.h"
#include <iostream>

class Background
{
	protected:
		Bitmap *mBitmap;
		RECT mrPosition;
		RECT mCollision;
		BOOL mBoolCol;

		virtual void CalcCollisionRect();

	public:
		Background(Bitmap *pBitmap, POINT ptPosition, BOOL bCollision);

		virtual ~Background();

		void Draw(HDC hdc, BOOL bTrans);
		void Update(int x, int y);
		
		RECT &getPosition() { return mrPosition; };
		BOOL getBoolCol() { return mBoolCol; };
		void setPosition(POINT ptPosition);
		int getWidth() {return mBitmap->getWidth();};
		int getHeight() {return mBitmap->getHeight();};
		RECT &getCollision() { return mCollision; };

		int getEvent() { return -3;};
		void setEnemyAnim(RECT r) {};
		void setEnemyAnimSec(RECT r) {};

};

inline void Background::CalcCollisionRect()
{
	int xShrink = (mrPosition.left - mrPosition.right)/12;
	int yShrink = (mrPosition.top - mrPosition.bottom)/12;
	CopyRect(&mCollision, &mrPosition);
	InflateRect(&mCollision, xShrink, yShrink);
}

inline void Background::setPosition(POINT ptPosition)
{
	OffsetRect(&mrPosition, ptPosition.x - mrPosition.left, ptPosition.y - mrPosition.top);

	if (mBoolCol == TRUE)
		CalcCollisionRect();
}

#endif