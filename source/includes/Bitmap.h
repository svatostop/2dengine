#ifndef BITMAP_H
#define BITMAP_H

#include <windows.h>

class Bitmap
{
protected:
  HBITMAP mhBitmap;
  int     miWidth, miHeight;

  void Free();

public:
  Bitmap();
  Bitmap(HDC hdc, LPTSTR szFileName);
  Bitmap(HDC hdc, UINT uiResID, HINSTANCE hInstance);
  Bitmap(HDC hdc, int iWidth, int iHeight, COLORREF crColor = RGB(0, 0, 0));
  virtual ~Bitmap();

  BOOL Create(HDC hdc, LPTSTR szFileName);
  BOOL Create(HDC hdc, UINT uiResID, HINSTANCE hInstance);
  BOOL Create(HDC hdc, int iWidth, int iHeight, COLORREF crColor);
  void Draw(HDC hdc, int x, int y, BOOL bTrans = FALSE, COLORREF crTransColor = RGB(255,0,255));
  int  getWidth() { return miWidth; };
  int  getHeight() { return miHeight; };
};

#endif