#ifndef BUILDER_H
#define BUILDER_H

#include <windows.h>
#include <iostream>
#include <sstream>
#include <unistd.h>
#include "..\res\Resource.h"
#include "GameEngine.h"
#include "Bitmap.h"
#include "Background.h"
#include "Player.h"
#include "Sprite.h"

#define A_KEY 0x41
#define D_KEY 0x44
#define S_KEY 0x53
#define W_KEY 0x57
#define E_KEY 0x45
#define F_KEY 0x46
#define P_KEY 0x50
#define Q_KEY 0x51

#define LEFT_SIDE 0
#define RIGHT_SIDE 1

void GameIntroDraw(HWND hWindowM);
void IntroDel();

extern HINSTANCE ghInstance;
extern GameEngine *gGame;
extern Bitmap *gGalaxyBitmap;
extern Bitmap *bmpFloor;
extern Bitmap *bmpCellSec;
extern Bitmap *bmpCell;
extern Bitmap *bmpCell2;
extern Bitmap *bmpCell3;
extern Bitmap *bmpPlayer;
extern Bitmap *bmpPlayer2;
extern Bitmap *bmpSprite1;
extern Bitmap *bmpSprite2;
extern Bitmap *bmpDoor;
extern Bitmap *bmpKey;
extern Bitmap *bmpHealth[3];
extern Player *pPlayer;
extern Player *pPlayerLeft;

extern RECT saveViewLoc;
extern RECT saveViewLocLeft;

extern int PlayerSide;

extern int secViewOffset;
extern BOOL flagOffset;

extern BOOL bGameViewDef;

extern HDC gOffsetScreenDC;
extern HBITMAP gOffsetScreenBitmap;

extern int checkLine;

extern BOOL bSecViewUpd;
extern BOOL bSecViewUpdDown;
#endif 