#ifndef GAMEENGINE_H
#define GAMEENGINE_H

#include <windows.h>

#include <mmsystem.h>
#include <iostream>
#include <vector>
#include <map>
#include <algorithm>
#include "Background.h"
#include "Sprite.h"
#include "Player.h"
#include "ParseParams.h"

#define GAME_STATE_INIT 0
#define GAME_STATE_RUN 1
#define GAME_STATE_RUN_SECVIEW 2
#define GAME_STATE_PAUSE 3
#define GAME_STATE_PAUSE_SECVIEW 4
#define GAME_STATE_TEXT 5
#define GAME_STATE_INIT_NXTLVL 6
#define GAME_NEXT_LVL 7
#define GAME_STATE_END 8
#define GAME_STATE_INTRO 9


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR szCmdLine, int iCmdShow);
LRESULT CALLBACK WndProc (HWND hWindow, UINT msg, WPARAM wParam, LPARAM lParam);

BOOL GameInit(HINSTANCE hInstance);
void GameMain();
void GameStart(HWND hWindow);
void GameStartSecLvl(HWND hWindow);
void GameEnd();
void GamePause(HWND hWindow);
void GameActivate(HWND hWindow);
void GameDeactivate(HWND hWindow);
void GamePaint(HDC hdc);
void GamePaintSecView(HDC hdc);
void GameCycle();
void GameCycleSecView();
void HandleKeys();
void HandleKeysSecView();
void WriteTextInit(HWND hWindow, int num);
int WriteText(HWND hWindow, HINSTANCE ghInstance, int num, int Loop);
void initTexts();

void GameIntroInit(HWND hWindow, HINSTANCE ghInstance);
// void HandlePauseMenu();

std::string getParamsFile();
std::string getFile(std::string &test);


class GameEngine
{
protected:
   static GameEngine* mGameEngine;
   HINSTANCE mhInstance;
   HWND mhWindow;
   TCHAR mszWindowsClass[32];
   TCHAR mszTitle[32];
   WORD mwIcon, mwSmallIcon;
   int miWidth, miHeght;
   int miFrameDelay;
   BOOL mbSleep;
   RECT mWindowBounds;
   std::vector<Background*> mBground;
   std::vector<Background*> mPlatforms;
   std::vector<Background*> mSecViewBground;
   std::vector<Sprite*> mSprites;
   std::vector<Sprite*> mEnemySprite;
      std::vector<Sprite*> mEnemySpriteSec;
   std::vector<Sprite*> mSpritesFirst;
   std::map<int, std::vector<std::string>> mSecView;
   RECT backRect;
   BOOL bGameViewDef;
   int mGameState;
   BOOL mTextCount;

   BOOL mBoolKeyJmp;

public:
   GameEngine(HINSTANCE hInstance, LPTSTR szWindowsClass, LPTSTR szTitle, WORD wIcon, WORD wSmallIcon, int iWidth, int iHeigh);
   virtual ~GameEngine();

   static GameEngine* getEngine() { return mGameEngine; };
   BOOL Initialize(int iCmdShow);
   LRESULT HandleEvent(HWND hWindow, UINT msg, WPARAM wParam, LPARAM lParam);

   void nextLevel();

   HINSTANCE getInstance() { return mhInstance; };
   HWND getWindow() { return mhWindow; };
   void setWindow(HWND hWindow) { mhWindow = hWindow; };
   LPTSTR getTitle() { return mszTitle; };
   WORD getIcon() { return mwIcon; };
   WORD getSmallIcon() { return mwSmallIcon; };
   int getWidth() { return miWidth; };
   int getHeight() { return miHeght; };
   int getGameState() { return mGameState;};
   void setGameState(int state) { mGameState = state;};
   int getTextCount() {return mTextCount;};
   void incTextCount() { mTextCount++;};
   int getFrameDelay() { return miFrameDelay; };
   void  setFrameRate(int iFrameRate) { miFrameDelay = 1000 / iFrameRate; };
   BOOL getSleep() { return mbSleep; };
   BOOL getGViewDef() { return bGameViewDef;};
   void setGViewDef(BOOL bB) { bGameViewDef = bB;};
   void setSleep(BOOL bSleep) {mbSleep = bSleep;};
   RECT &getWinBounds() { return mWindowBounds; };
   std::vector<Background*> &getVbg() { return mBground; };
   std::vector<Background*> &getVPlatforms() { return mPlatforms; };
   std::vector<Background*> &getSecVbg() { return mSecViewBground; };
   std::vector<Sprite*> &getVSprite() { return mSprites; };
   std::vector<Sprite*> &getVFirstSprite() { return mSpritesFirst; };
   std::vector<Sprite*> &getEnemySprite() { return mEnemySprite;};
   std::vector<Sprite*> &getEnemySpriteSec() { return mEnemySpriteSec;};

   BOOL updateBgFrame(int x, int y, Player *pl, RECT &oldPos);
   BOOL checkBounds(Player *pl, RECT &oldPos);
   void updateSecView(int x, int y);
   BOOL checkEnemyBounds(Player *pl, RECT &oldPos);
   void updateSecViewSprite(int upd);
   void updateEnemyAnimSprite();
   BOOL checkBoundsForEnemy(Sprite *pl, RECT &oldPos);
   BOOL  updateBgFrSec(int x, int y, Player *pl,  RECT &oldPos);
void updateSecViewSpriteEnemy(int upd);
void updateEnemyAnimSpriteSec();
BOOL checkEnemyBoundsSec(Player *pl, RECT &oldPos);

BOOL getJmpKey() { return mBoolKeyJmp;};
void setJmpKey(BOOL b) {mBoolKeyJmp = b;};

   std::string getMap();
   int getYMapLen();
   std::map<int, std::vector<std::string>> getMView() { return mSecView; };

   template<typename T, typename Arr> void addBackground(T *bg, Arr &curArray);
   template<typename T, typename Arr> void drawBackground(HDC hdc, BOOL bTrans, Arr &curArray);
   template<typename T, typename Arr> void cleanupBG(Arr &curArray);
   template<typename T, typename Arr> void updateSecView(int x, int y, Arr &curArray);
   template<typename Arr> BOOL checkBoundsSprite(Player *pl, RECT &oldPos,  Arr &curArray);
   template<typename Arr> BOOL updateSpriteFrame(int x, int y, Player *pl,  RECT &oldPos,Arr &curArray);

};

template<typename Arr> BOOL GameEngine::updateSpriteFrame(int x, int y, Player *pl,  RECT &oldPos,Arr &curArray)
{
   std::vector<Sprite *>::iterator spriteIt;
   RECT tmpPos = pl->getPosition();
   RECT bounds = getWinBounds();

   for (spriteIt = curArray.begin(); spriteIt != curArray.end(); spriteIt++)
   {
      if ((*spriteIt)->getEvent() == ENEMY || (*spriteIt)->getEvent() == HIDE_PLAT )
      {
         //RECT animPosCheck = (*spriteIt)->getEnemyAnim();
         if ((*spriteIt)->getEvent() != HIDE_PLAT)
         {
            RECT animPos = (*spriteIt)->getEnemyAnim();

            (*spriteIt)->Update(x,y);

            animPos.left += x;

            animPos.top += y;

            (*spriteIt)->setEnemyAnim(animPos);
            animPos.top = 280;
            (*spriteIt)->setEnemyAnimSec(animPos);
         }
         else
         {

            RECT animPos = (*spriteIt)->getEnemyAnimSec();

            (*spriteIt)->Update(x,y);

            animPos.left += x;

            (*spriteIt)->setEnemyAnimSec(animPos);
         }
      }
      else
         (*spriteIt)->Update(x, y);

      if (pl->testCollSprites((*spriteIt)) || tmpPos.left < bounds.left
         || tmpPos.left > bounds.right || tmpPos.top < bounds.top
         || tmpPos.top > bounds.bottom)
      {
         pl->setPosition({oldPos.left, oldPos.top});
      }
   }
}

template<typename Arr> BOOL GameEngine::checkBoundsSprite(Player *pl, RECT &oldPos, Arr &curArray)
{
   std::vector<Sprite *>::iterator spriteIt;
  
   for (spriteIt = curArray.begin(); spriteIt != curArray.end(); spriteIt++)
   {
      if (((*spriteIt)->getEvent() != HIDE && (*spriteIt)->getEvent() != ATTACK) && (*spriteIt)->getBoolCol() == TRUE)
      {
         if (pl->testCollSprites((*spriteIt)))
         {

            pl->setPosition({oldPos.left, oldPos.top});

            if ((*spriteIt)->getEvent() == ENEMY)
            {
               pl->decHealth();
               pl->setPosition({oldPos.left - 80, oldPos.top});
               if (pl->getHealth() == 0)
                  GameEngine::getEngine()->setGameState(GAME_STATE_END);
            }
            if ((*spriteIt)->getEvent() == ON_TEXT)
            {
               GameEngine::getEngine()->setGameState(GAME_STATE_TEXT);
               ((*spriteIt)->setEvent(OFF_TEXT));
            }
            else if ((*spriteIt)->getEvent() == NEXT_LVL
               || ((*spriteIt)->getEvent() == NEXT_LVL_KEY && pl->getKey() == TRUE))
            {
               GameEngine::getEngine()->setGameState(GAME_STATE_INIT_NXTLVL);
               ((*spriteIt)->setEvent(OFF));
            }
            if ((*spriteIt)->getEvent() == KEY && (GameEngine::getEngine()->getJmpKey() == TRUE))
            {
               pl->setKey(TRUE);
               ((*spriteIt)->setEvent(HIDE));
            }
            return TRUE;
         }
      }
   }
   return FALSE;
}

template<typename T, typename Arr> void GameEngine::addBackground(T *bg, Arr &curArray)
{
   if (bg != NULL)
   {
      curArray.push_back(bg);
      if (bg->getEvent() == ENEMY)
      {
         RECT animPos = bg->getPosition();
         bg->setEnemyAnim(animPos);

         animPos.top = 280;
         bg->setEnemyAnimSec(animPos);
      }
      if (bg->getEvent() == HIDE_PLAT)
      {
         RECT animPos = bg->getPosition();
         bg->setEnemyAnimSec(animPos);
      }
   }
}

template<typename T, typename Arr> void GameEngine::drawBackground(HDC hdc,BOOL bTrans, Arr &curArray)
{
   typename std::vector<T *>::iterator spriteIt;
   for (spriteIt = curArray.begin(); spriteIt != curArray.end(); spriteIt++)
   {
      if ((*spriteIt)->getEvent() != HIDE && (*spriteIt)->getEvent() != HIDE_PLAT)
         (*spriteIt)->Draw(hdc, bTrans);
   }

}

template<typename T, typename Arr> void GameEngine::cleanupBG(Arr &curArray)
{
   typename std::vector<T *>::iterator spriteIt;
   for (spriteIt = curArray.begin(); spriteIt != curArray.end(); spriteIt++)
   {
      delete (*spriteIt);
      curArray.erase(spriteIt);
      spriteIt--;
   }
}

template<typename T, typename Arr> void GameEngine::updateSecView(int x, int y, Arr &curArray)
{
   typename std::vector<T *>::iterator spriteIt;

   for (spriteIt = curArray.begin(); spriteIt != curArray.end(); spriteIt++)
   {
      (*spriteIt)->Update(x, y);
   }
}

#endif