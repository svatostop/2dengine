#ifndef PARSEPARAMS_H
#define PARSEPARAMS_H

#include <iostream>
#include <windows.h>
#include <string>
#include <map>
#include <vector>
#include <utility>
#include <sstream>

void printError( char * res);
void printDbg(const char * res);

class mainParams
{
	protected:
		int w;
		int h;
		int mode;
		int yLen;
		std::string map;
		std::string mapFirstLvl;
		std::string mapSecondLvl;
		std::string map3Lvl;
		std::string map4Lvl;
		std::string map5Lvl;
		std::string str;

	public :
		mainParams();

		virtual ~mainParams() {};

		std::map<int, std::vector<std::string>> parseMap;

		void setFile(std::string s) { this->str.append(s); };
		void setRes();
		void setMap();
		void setMapView();
		int getYLen() { return this->yLen; };
		int getResW() { return this->w; };
		int getResH() { return this->h; };
		int getMapMode() ;
		std::string getMapStr() { return this->map; };
		std::map<int, std::vector<std::string>> getMapView() { return this->parseMap; };
};


#endif