#ifndef PLAYER_H
#define PLAYER_H

#include <windows.h>
#include "Bitmap.h"
#include "Background.h"
#include "Sprite.h"
#include <iostream>


class Player
{
	protected:
		Bitmap *mBitmap;
		RECT mrPosition;
		RECT mCollision;
		BOOL mJumpActBegin;
		BOOL mJumpActEnd;
		int mJumpScale;
		int mJumpScaleEnd;
		static int mHealth;

		BOOL mJumpStart;
		BOOL mKey;

		virtual void CalcCollisionRect();

	public:
		Player(Bitmap *pBitmap, POINT ptPosition);

		virtual ~Player();


		void Draw(HDC hdc);
		void Update(int x, int y);
		void JumpUpdate(int x, int y);
		BOOL testCollision(Background *testSprite);
		BOOL testCollSprites(Sprite *testSprite);

		
		RECT &getPosition() { return mrPosition; };
		void setPosition(POINT ptPosition);
		void setJump(BOOL bB) { mJumpActBegin = bB;};
		void setJumpEnd(BOOL bB) { mJumpActEnd = bB;};
		void setJumpStart(BOOL bB) { mJumpStart = bB;};
		BOOL getJumpStart() { return mJumpStart;};
		void setJumpScale(int y) { mJumpScale = y;};
		void setJumpScaleEnd(int y) { mJumpScaleEnd = y;};
		int getJumpScale() { return mJumpScale;};
		int getJumpScEnd() { return mJumpScaleEnd;};

		BOOL getKey() {return mKey;};
		void setKey(BOOL b) { mKey = b;};

		int getHealth() { return Player::mHealth;};
		void setHealth(int h) { Player::mHealth = h;};
		void incHealth() { Player::mHealth++;};
		void decHealth() { Player::mHealth--;};

		BOOL jumpEvent() { return mJumpActBegin;};
		BOOL jumpEventEnd() { return mJumpActEnd;};
		int getWidth() {return mBitmap->getWidth();};
		int getHeight() {return mBitmap->getHeight();};
		RECT &getCollision() { return mCollision; };
};

inline void Player::CalcCollisionRect()
{
	int xShrink = (mrPosition.left - mrPosition.right)/12;
	int yShrink = (mrPosition.top - mrPosition.bottom)/12;
	CopyRect(&mCollision, &mrPosition);
	InflateRect(&mCollision, xShrink, yShrink);
}

inline BOOL Player::testCollision(Background *testSprite)
{
	RECT &testRect = testSprite->getCollision();
	return mCollision.left <= testRect.right && testRect.left <= mCollision.right &&
	mCollision.top <= testRect.bottom && testRect.top <= mCollision.bottom;

}

inline BOOL Player::testCollSprites(Sprite *testSprite)
{
	RECT &testRect = testSprite->getCollision();
	return mCollision.left <= testRect.right && testRect.left <= mCollision.right &&
	mCollision.top <= testRect.bottom && testRect.top <= mCollision.bottom;

}

inline void Player::setPosition(POINT ptPosition)
{
	OffsetRect(&mrPosition, ptPosition.x - mrPosition.left, ptPosition.y - mrPosition.top);
	CalcCollisionRect();
}



#endif