#ifndef SPRITE_H
#define SPRITE_H

#include <windows.h>
#include "Bitmap.h"
#include "Background.h"
#include <iostream>

#define ENEMY -1
#define OFF 0
#define ON_TEXT 1
#define OFF_TEXT 2
#define NEXT_LVL 3
#define HIDE 4
#define ATTACK 5
#define NEXT_LVL_KEY 6
#define ENEMY_PLAT 7
#define HIDE_PLAT 8
#define FLOOR 9
#define KEY 10


class Sprite
{
	protected:
		Bitmap *mBitmap;
		RECT mrPosition;
		RECT mCollision;
		BOOL mBoolCol;

		RECT sEnemyPos;
		RECT sEnemyAnim;
		RECT sEnemyAnimSecV;

		int mEvent;
		int mHealth;
		BOOL bAnim;
		BOOL bAnimSec;


		virtual void CalcCollisionRect();

	public:
		Sprite(Bitmap *pBitmap, POINT ptPosition, BOOL bCollision = TRUE, int Event = OFF);

		virtual ~Sprite();

		void Draw(HDC hdc, BOOL bTrans);
		void Update(int x, int y);
		
		RECT &getPosition() { return mrPosition; };
		void saveEnemyPos(RECT saveP) { sEnemyPos = saveP;};
		RECT &getEnemyPos() { return sEnemyPos;};
		void setEnemyAnim(RECT cur) { sEnemyAnim = cur;};
		RECT &getEnemyAnim() { return sEnemyAnim;};
		RECT &getEnemyAnimSec() { return sEnemyAnimSecV;};
		void setEnemyAnimSec(RECT y) { sEnemyAnimSecV = y;};
		void setAnimDown(int b) { bAnim = b;};
		int getAnimDown() {return bAnim;};
		void setAnimDownSec(BOOL b) { bAnimSec = b;};
		BOOL getAnimDownSec() {return bAnimSec;};

		void setPosition(POINT ptPosition);
		BOOL getBoolCol() {return mBoolCol;};
		int getWidth() {return mBitmap->getWidth();};
		int getHeight() {return mBitmap->getHeight();};
		RECT &getCollision() { return mCollision; };
		int getEvent() { return mEvent;};
		void setEvent(int t) { mEvent = t;};

		BOOL testCollision(Background *testSprite);

		int getHealth() { return mHealth;};
		void decHealth() { mHealth--;};
};

inline void Sprite::CalcCollisionRect()
{
	int xShrink = (mrPosition.left - mrPosition.right)/12;
	int yShrink = (mrPosition.top - mrPosition.bottom)/12;
	CopyRect(&mCollision, &mrPosition);
	InflateRect(&mCollision, xShrink, yShrink);
}

inline BOOL Sprite::testCollision(Background *testSprite)
{
	RECT &testRect = testSprite->getCollision();
	return mCollision.left <= testRect.right && testRect.left <= mCollision.right &&
	mCollision.top <= testRect.bottom && testRect.top <= mCollision.bottom;

}

inline void Sprite::setPosition(POINT ptPosition)
{
	OffsetRect(&mrPosition, ptPosition.x - mrPosition.left, ptPosition.y - mrPosition.top);
	CalcCollisionRect();
}

#endif