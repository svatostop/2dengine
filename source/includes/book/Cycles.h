#ifndef CYCLES_H
#define CYCLES_H

#include <windows.h>
#include "..\res\Resource.h"
#include "GameEngine.h"
#include "Bitmap.h"
#include <iostream>

HINSTANCE ghInstance;
GameEngine *gpGame;
Bitmap *gpBackground;
Bitmap *gpCycle[2][4];
POINT gptCyclePos[2];
POINT gptCycleSpeed[2];
POINT gptCycleTrail[2][100];
int giTrailLen[2];
const int giSpeed = 4;
BOOL gbGameOver;

void NewGame();
void updateCycles();
void steerCycle(int iCycle, int iDir);
void EndGame(int iCycle);

#endif