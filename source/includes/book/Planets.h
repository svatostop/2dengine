#ifndef PLANETS_H
#define PLANETS_H

#include <windows.h>
#include <iostream>
#include <sstream>
#include "..\res\Resource.h"
#include "GameEngine.h"
#include "Bitmap.h"
#include "Background.h"
#include "Player.h"

HINSTANCE ghInstance;
GameEngine *gpGame;
Bitmap *gpGalaxyBitmap;
Bitmap *floor;
Bitmap *cell;
Bitmap *player;
Bitmap *gpPlanetBitmap[3];
Player *pPlayer;

HDC gOffsetScreenDC;
HBITMAP gOffsetScreenBitmap;


#endif 