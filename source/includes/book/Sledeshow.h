#ifndef SLIDESHOW_H
#define SLIDESHOW_H

#include <windows.h>
#include "..\res\Resource.h"
#include "GameEngine.h"
#include "Bitmap.h"


//-----------------------------------------------------------------
// Global Variables
//-----------------------------------------------------------------
HINSTANCE   g_hInstance;
GameEngine* g_pGame;
const int   g_iNUMSLIDES = 6;
Bitmap*     g_pSlides[g_iNUMSLIDES];
int         g_iCurSlide;


#endif