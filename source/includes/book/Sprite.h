#ifndef SPRITE_H
#define SPRITE_H

#include <windows.h>
#include "Bitmap.h"
#include <iostream>

typedef WORD SPRITEACTION;
const SPRITEACTION
SA_NONE = 0x0000L,
SA_KILL = 0x0001L;

typedef WORD BOUNDSACTION;
const BOUNDSACTION
BA_STOP   = 0,
BA_WRAP   = 1,
BA_BOUNCE = 2,
BA_DIE    = 3;


class Sprite
{
protected:
	Bitmap *mBitmap;
	RECT mrPosition, mCollision;
	POINT mpVelocity;
	int miZorder;
	RECT mrBounds;
	BOUNDSACTION mbBoundsAction;
	BOOL mbHidden;

	virtual void CalcCollisionRect();

public:
	Sprite(Bitmap *pBitmap);
	Sprite(Bitmap *pBitmap, RECT &rBounds, BOUNDSACTION bBoundsAction = BA_STOP);
	Sprite(Bitmap *pBitmap, POINT ptPosition, POINT pVelocity, int iZorder, RECT &rBounds, BOUNDSACTION bBoundsAction = BA_STOP);

	virtual ~Sprite();

	virtual SPRITEACTION Update();
	void Draw(HDC hdc);
	BOOL isPointInside(int x, int y);
	BOOL testCollision(Sprite *testSprite);
	
	RECT &getPosition() { return mrPosition; };
	void setPosition(int x, int y);
	void setPosition(POINT ptPosition);
	void setPosition(RECT &rPosition) { CopyRect(&mrPosition, &rPosition); };
	void offsetPosition(int x, int y);
	POINT getVelocity() { return mpVelocity; };
	void setVelocity(int x, int y);
	void setVelocity(POINT ptVelocity);
	BOOL getZorder() { return miZorder;};
	void setZorder(int iZorder) { miZorder = iZorder;};
	void setBounds(RECT &rBounds) { CopyRect(&mrBounds, &rBounds);};
	void setBoundsAction(BOUNDSACTION bAction) { mbBoundsAction = bAction;};
	BOOL isHidden() {return mbHidden;};
	void setHidden(BOOL bHidden) {mbHidden = bHidden;};
	int getWidth() {return mBitmap->getWidth();};
	int getHeight() {return mBitmap->getHeight();};

	RECT &getCollision() { return mCollision; };

};


inline void Sprite::CalcCollisionRect()
{
	int xShrink = (mrPosition.left - mrPosition.right)/12;
	int yShrink = (mrPosition.top - mrPosition.bottom)/12;
	CopyRect(&mCollision, &mrPosition);
	InflateRect(&mCollision, xShrink, yShrink);
}

inline BOOL Sprite::testCollision(Sprite *testSprite)
{
	RECT &testRect = testSprite->getCollision();
	return mCollision.left <= testRect.right && testRect.left <= mCollision.right &&
	mCollision.top <= testRect.bottom && testRect.top <= mCollision.bottom;

}

inline BOOL Sprite::isPointInside(int x, int y)
{
	POINT point;
	point.x = x;
	point.y = y;
	return PtInRect(&mrPosition, point);
}

inline void Sprite::setPosition(int x, int y)
{
	OffsetRect(&mrPosition, x - mrPosition.left, y - mrPosition.top);
	CalcCollisionRect();
}

inline void Sprite::setPosition(POINT ptPosition)
{
	OffsetRect(&mrPosition, ptPosition.x - mrPosition.left, ptPosition.y - mrPosition.top);
	CalcCollisionRect();
}

inline void Sprite::offsetPosition(int x, int y)
{
	OffsetRect(&mrPosition,x ,y);
	CalcCollisionRect();
}

inline void Sprite::setVelocity(int x, int y)
{
	mpVelocity.x = x;
	mpVelocity.y = y;
}

inline void Sprite::setVelocity(POINT ptVelocity)
{
	mpVelocity.x = ptVelocity.x;
	mpVelocity.y = ptVelocity.y;
}


#endif