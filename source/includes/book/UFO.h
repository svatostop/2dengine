#ifndef UFO_H
#define UFO_H

#include <windows.h>
#include <iostream>
#include "..\res\Resource.h"
#include "GameEngine.h"
#include "Bitmap.h"

HINSTANCE ghInstance;
GameEngine *gpGame;
const int giMaxSpeed = 8;
Bitmap *gpBackground;
Bitmap *gpSaucer;
int giSaucerX, giSaucerY;
int giSpeedX, giSpeedY;

#endif