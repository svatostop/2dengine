#ifndef ENGINEHANDLE_H
#define ENGINEHANDLE_H

#include <windows.h>
#include "..\includes\parseParams.h"

HWND mHwnd; // Main Window Handle
RECT clientRect;
HBITMAP pattern;
HBITMAP kitty;
HINSTANCE hInst; 
BLENDFUNCTION blf = {AC_SRC_OVER, 0, 255, 0};
bool forward = true;
unsigned char alpha = 0;
HDC hdc;
bool isDrawing = true;
HDC backDC;
HBITMAP backBMP;

LRESULT CALLBACK WndProc (HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void drawFrame();
mainParams parser();

#endif
