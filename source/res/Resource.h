//-----------------------------------------------------------------
// Icons                    Range : 1000 - 1999
//-----------------------------------------------------------------
#define IDI_SLIDESHOW             1000
#define IDI_SLIDESHOW_SM          1001

//-----------------------------------------------------------------
// Bitmaps                  Range : 2000 - 2999
//-----------------------------------------------------------------
#define IDB_GALAXY 2000
#define IDB_CELL 2001
#define IDB_PLAYER 2002
#define IDB_CELL2 2003
#define IDB_CELL3 2004
#define IDB_SPRITE1 2005
#define IDB_FLOOR1 2006
#define IDB_CELLSEC 2007
#define IDB_PLAYER2 2008
#define IDB_SPRITE2 2009
#define IDB_NEXT 2010
#define IDB_BG2 2011
#define IDB_PLAYER2LVL 2012
#define IDB_PLAYER2LVL2 2013
#define IDB_FLOOR2 2014
#define IDB_HEALTH 2015
#define IDB_SPRITE3 2016
#define IDB_SPRITE4 2017
#define IDB_INTRO 2018
#define IDB_PLAYER3LVL 2019
#define IDB_PLAYER3LVL2 2020
#define IDB_DOOR 2021
#define IDB_KEY 2022
#define IDB_WALL2 2023
#define IDB_WALL3 2024
