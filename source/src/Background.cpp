#include "..\includes\Background.h"

Background::Background(Bitmap *pBitmap, POINT ptPosition, BOOL bCollision)
{
	mBitmap = pBitmap;
	SetRect(&mrPosition, ptPosition.x, ptPosition.y, ptPosition.x + pBitmap->getWidth(), ptPosition.y + pBitmap->getHeight());
	mBoolCol = bCollision;

	if (mBoolCol == TRUE)
		CalcCollisionRect();
}


Background::~Background()
{
}

void Background::Draw(HDC hdc, BOOL bTrans)
{
	if (mBitmap != NULL)
		mBitmap->Draw(hdc, mrPosition.left, mrPosition.top, bTrans);
}

void Background::Update(int x, int y)
{
	POINT newPos;

	newPos.x = mrPosition.left + x;
	newPos.y = mrPosition.top + y;

	setPosition(newPos);
}