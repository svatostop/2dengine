
#include "..\includes\Bitmap.h"
Bitmap::Bitmap() : mhBitmap(NULL), miWidth(0), miHeight(0)
{
}

Bitmap::Bitmap(HDC hdc, LPTSTR szFileName)
  : mhBitmap(NULL), miWidth(0), miHeight(0)
{
  Create(hdc, szFileName);
}

Bitmap::Bitmap(HDC hdc, UINT uiResID, HINSTANCE hInstance)
  : mhBitmap(NULL), miWidth(0), miHeight(0)
{
  Create(hdc, uiResID, hInstance);
}

Bitmap::Bitmap(HDC hdc, int iWidth, int iHeight, COLORREF crColor)
  : mhBitmap(NULL), miWidth(0), miHeight(0)
{
  Create(hdc, iWidth, iHeight, crColor);
}

Bitmap::~Bitmap()
{
  Free();
}

void Bitmap::Free()
{
  if (mhBitmap != NULL)
  {
    DeleteObject(mhBitmap);
    mhBitmap = NULL;
  }
}

BOOL Bitmap::Create(HDC hdc, LPTSTR szFileName)
{
  Free();

  HANDLE hFile = CreateFile(szFileName, GENERIC_READ, FILE_SHARE_READ, NULL,
    OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
  if (hFile == INVALID_HANDLE_VALUE)
    return FALSE;

  BITMAPFILEHEADER  bmfHeader;
  DWORD             dwBytesRead;
  BOOL bOK = ReadFile(hFile, &bmfHeader, sizeof(BITMAPFILEHEADER),
    &dwBytesRead, NULL);
  if ((!bOK) || (dwBytesRead != sizeof(BITMAPFILEHEADER)) ||
    (bmfHeader.bfType != 0x4D42))
  {
    CloseHandle(hFile);
    return FALSE;
  }

  BITMAPINFO* pBitmapInfo = (new BITMAPINFO);
  if (pBitmapInfo != NULL)
  {
    bOK = ReadFile(hFile, pBitmapInfo, sizeof(BITMAPINFOHEADER),
      &dwBytesRead, NULL);
    if ((!bOK) || (dwBytesRead != sizeof(BITMAPINFOHEADER)))
    {
      CloseHandle(hFile);
      Free();
      return FALSE;
    }

    miWidth = (int)pBitmapInfo->bmiHeader.biWidth;
    miHeight = (int)pBitmapInfo->bmiHeader.biHeight;

    PBYTE pBitmapBits;
    mhBitmap = CreateDIBSection(hdc, pBitmapInfo, DIB_RGB_COLORS,
      (PVOID*)&pBitmapBits, NULL, 0);
    if ((mhBitmap != NULL) && (pBitmapBits != NULL))
    {
      SetFilePointer(hFile, bmfHeader.bfOffBits, NULL, FILE_BEGIN);
      bOK = ReadFile(hFile, pBitmapBits, pBitmapInfo->bmiHeader.biSizeImage,
        &dwBytesRead, NULL);
      if (bOK)
        return TRUE;
    }
  }

  Free();
  return FALSE;
}

BOOL Bitmap::Create(HDC hdc, UINT uiResID, HINSTANCE hInstance)
{
  Free();

  HRSRC hResInfo = FindResource(hInstance, MAKEINTRESOURCE(uiResID), RT_BITMAP);
  if (hResInfo == NULL)
    return FALSE;

  HGLOBAL hMemBitmap = LoadResource(hInstance, hResInfo);
  if (hMemBitmap == NULL)
    return FALSE;

  PBYTE pBitmapImage = (BYTE*)LockResource(hMemBitmap);
  if (pBitmapImage == NULL)
  {
    FreeResource(hMemBitmap);
    return FALSE;
  }

  BITMAPINFO* pBitmapInfo = (BITMAPINFO*)pBitmapImage;
  miWidth = (int)pBitmapInfo->bmiHeader.biWidth;
  miHeight = (int)pBitmapInfo->bmiHeader.biHeight;

  PBYTE pBitmapBits;
  mhBitmap = CreateDIBSection(hdc, pBitmapInfo, DIB_RGB_COLORS,
    (PVOID*)&pBitmapBits, NULL, 0);
  if ((mhBitmap != NULL) && (pBitmapBits != NULL))
  {
    const PBYTE pTempBits = pBitmapImage + pBitmapInfo->bmiHeader.biSize +
      pBitmapInfo->bmiHeader.biClrUsed * sizeof(RGBQUAD);
    CopyMemory(pBitmapBits, pTempBits, pBitmapInfo->bmiHeader.biSizeImage);

    UnlockResource(hMemBitmap);
    FreeResource(hMemBitmap);
    return TRUE;
  }

  UnlockResource(hMemBitmap);
  FreeResource(hMemBitmap);
  Free();
  return FALSE;
}

BOOL Bitmap::Create(HDC hdc, int iWidth, int iHeight, COLORREF crColor)
{
  mhBitmap = CreateCompatibleBitmap(hdc, iWidth, iHeight);
  if (mhBitmap == NULL)
    return FALSE;

  miWidth = iWidth;
  miHeight = iHeight;

  HDC memDc = CreateCompatibleDC(hdc);

  HBRUSH hBrush = CreateSolidBrush(crColor);

  HBITMAP oldBitmap = (HBITMAP)SelectObject(memDc, mhBitmap);

  RECT rcBitmap = { 0, 0, miWidth, miHeight };
  FillRect(memDc, &rcBitmap, hBrush);

  SelectObject(memDc, oldBitmap);
  DeleteDC(memDc);
  DeleteObject(hBrush);

  return TRUE;
}

void Bitmap::Draw(HDC hdc, int x, int y, BOOL bTrans, COLORREF crTransColor)
{
  if (mhBitmap != NULL)
  {
    HDC memDc = CreateCompatibleDC(hdc);

    HBITMAP oldBitmap = (HBITMAP)SelectObject(memDc, mhBitmap);

    if (bTrans)
      TransparentBlt(hdc, x, y, getWidth(), getHeight(), memDc, 0,0, getWidth(), getHeight(), crTransColor);
    else
      BitBlt(hdc, x, y, getWidth(), getHeight(), memDc, 0, 0, SRCCOPY);

    SelectObject(memDc, oldBitmap);
    DeleteDC(memDc);
  }
}
