#include "..\includes\Builder.h"

BOOL GameInit(HINSTANCE hInstance)
{
	gGame = new GameEngine(hInstance, TEXT("Planets"), TEXT("Planets"), IDI_SLIDESHOW, IDI_SLIDESHOW_SM, 500, 500);
	if (gGame == NULL)
		return FALSE;

	gGame->setFrameRate(30);

	ghInstance = hInstance;

	return TRUE;
}

void GameMain()
{
	switch(gGame->getGameState())
	{
		case GAME_STATE_INIT:
		{
			WriteTextInit(gGame->getWindow(), 0);
			if (GetAsyncKeyState(VK_RETURN) < 0)
				gGame->setGameState(GAME_STATE_RUN);
			
		} break ;
		case GAME_STATE_RUN:
		{
			HandleKeys();
      		GameCycle();

			if (GetAsyncKeyState(P_KEY) < 0)
				gGame->setGameState(GAME_STATE_PAUSE);
		} break ;
		case GAME_STATE_RUN_SECVIEW:
		{
			HandleKeysSecView();
			GameCycleSecView();
			if (GetAsyncKeyState(P_KEY) < 0)
				gGame->setGameState(GAME_STATE_PAUSE_SECVIEW);
		} break ;
		case GAME_STATE_PAUSE:
		{
			GamePause(gGame->getWindow());
			if (GetAsyncKeyState(VK_ESCAPE) < 0)
				gGame->setGameState(GAME_STATE_RUN);
			if (GetAsyncKeyState(Q_KEY) < 0)
				gGame->setGameState(GAME_STATE_END);

		} break ;
		case GAME_STATE_PAUSE_SECVIEW:
		{
			GamePause(gGame->getWindow());
			if (GetAsyncKeyState(VK_ESCAPE) < 0)
				gGame->setGameState(GAME_STATE_RUN_SECVIEW);
			if (GetAsyncKeyState(Q_KEY) < 0)
				gGame->setGameState(GAME_STATE_END);

		} break ;
		case GAME_STATE_TEXT:
		{
			checkLine = WriteText(gGame->getWindow(), ghInstance, gGame->getTextCount(), 0);
			Sleep(100);
			if (checkLine == 0 && GetAsyncKeyState(VK_RETURN) < 0)
			{
				checkLine =  WriteText(gGame->getWindow(), ghInstance, gGame->getTextCount(), 1);
			}
			if (checkLine == 1 && GetAsyncKeyState(VK_RETURN) < 0)
			{
				gGame->setGameState(GAME_STATE_RUN);
				gGame->incTextCount();
			}
		} break ;
		case GAME_STATE_INIT_NXTLVL:
		{
			WriteTextInit(gGame->getWindow(), 1);
			if (GetAsyncKeyState(VK_RETURN) < 0)
				gGame->setGameState(GAME_NEXT_LVL);
			
		} break ;
		case GAME_NEXT_LVL:
		{
			GameEnd();
			gGame->nextLevel();
			GameStartSecLvl(gGame->getWindow());
			gGame->setGameState(GAME_STATE_RUN);
		} break ;
		case GAME_STATE_INTRO:
		{
			GameIntroDraw(gGame->getWindow());
			if (GetAsyncKeyState(VK_SPACE) < 0)
			{
				IntroDel();
				gGame->setGameState(GAME_STATE_INIT);
			}
		}
	}
}


void GameEnd()
{
	DeleteObject(gOffsetScreenBitmap);
	DeleteDC(gOffsetScreenDC);

	delete gGalaxyBitmap;

	delete bmpFloor;
	delete bmpSprite1;
	delete bmpSprite2;
	delete bmpCell2;
	delete bmpCell3;
	delete bmpCell;
	delete bmpCellSec;
	delete bmpPlayer;
	delete bmpPlayer2;
	delete bmpDoor;



	for (int i = 0; i < 3; i++)
	{
		delete bmpHealth[i];
	}

	gGame->cleanupBG<Background, std::vector<Background*>>(gGame->getVbg());
	gGame->cleanupBG<Background, std::vector<Background*>>(gGame->getSecVbg());
	gGame->cleanupBG<Background, std::vector<Background*>>(gGame->getVPlatforms());
	gGame->cleanupBG<Sprite,  std::vector<Sprite*>>(gGame->getVFirstSprite());
	gGame->cleanupBG<Sprite, std::vector<Sprite*>>(gGame->getEnemySprite());
	gGame->cleanupBG<Sprite, std::vector<Sprite*>>(gGame->getEnemySpriteSec());
	gGame->cleanupBG<Sprite,  std::vector<Sprite*>>(gGame->getVSprite());
	delete pPlayer;
	delete pPlayerLeft;
	if (bmpKey != NULL)
		delete bmpKey;

	if (gGame->getGameState() != GAME_NEXT_LVL)
		delete gGame;
}

void GameActivate(HWND hWindow)
{
}

void GameDeactivate(HWND hWindow)
{
}

void GamePause(HWND hWindow)
{
	HDC   hdc;
	RECT  rect;

	SetRect(&rect, 0,50,gGame->getWidth(), gGame->getHeight()/2);
	hdc = GetDC(hWindow);

	Bitmap *paused = new Bitmap(hdc, gGame->getWidth(), gGame->getHeight(), RGB(0,0,0));

	paused->Draw(hdc, 0, 0);
	HFONT hfont =  CreateFont( -36, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, "GOST 2.304-81");
	SelectObject(hdc,hfont);
	SetTextColor(hdc, RGB(0, 255, 0));
	SetBkMode(hdc, TRANSPARENT);
	int height = DrawText(hdc, TEXT("Paused"), -1, &rect,
	DT_SINGLELINE | DT_CENTER | DT_VCENTER);
	OffsetRect(&rect, 0, height);
	height = DrawText(hdc, TEXT("Press Esc"), -1, &rect,
	DT_SINGLELINE | DT_CENTER | DT_VCENTER);
	OffsetRect(&rect, 0, height);
	SetTextColor(hdc, RGB(255, 255, 255));
	height = DrawText(hdc, TEXT("To exit press Q"), -1, &rect,
	DT_SINGLELINE | DT_CENTER | DT_VCENTER);
	DeleteObject(hfont);
	delete paused;
	ReleaseDC(hWindow, hdc);
}


void GamePaint(HDC hdc)
{
	gGalaxyBitmap->Draw(hdc, 0 ,0);
	
	gGame->drawBackground<Background, std::vector<Background*>>(hdc,FALSE, gGame->getVbg());
	gGame->drawBackground<Sprite, std::vector<Sprite*>>(hdc, TRUE, gGame->getVFirstSprite());
	gGame->updateEnemyAnimSprite();	
	gGame->drawBackground<Sprite, std::vector<Sprite*>>(hdc, TRUE, gGame->getEnemySprite());


	if (PlayerSide == RIGHT_SIDE)
		pPlayer->Draw(hdc);
	else if (PlayerSide == LEFT_SIDE)
		pPlayerLeft->Draw(hdc);

	gGame->drawBackground<Background, std::vector<Background*>>(hdc,FALSE, gGame->getVPlatforms());
	int x = 10;
	for (int i = 0; i < pPlayer->getHealth(); i++)
	{
		bmpHealth[i]->Draw(hdc, x, 10, TRUE);
		x += 15;
	}

}

void GamePaintSecView(HDC hdc)
{
	gGalaxyBitmap->Draw(hdc, 0 ,0);
	
	gGame->drawBackground<Background, std::vector<Background*>>(hdc, FALSE, gGame->getSecVbg());
	gGame->drawBackground<Sprite, std::vector<Sprite*>>(hdc, TRUE, gGame->getVSprite());
	gGame->updateEnemyAnimSprite();
	gGame->drawBackground<Sprite, std::vector<Sprite*>>(hdc, TRUE, gGame->getEnemySprite());
	gGame->updateEnemyAnimSpriteSec();
	gGame->drawBackground<Sprite, std::vector<Sprite*>>(hdc, TRUE, gGame->getEnemySpriteSec());
	
	if (PlayerSide == RIGHT_SIDE)
		pPlayer->Draw(hdc);
	else if (PlayerSide == LEFT_SIDE)
		pPlayerLeft->Draw(hdc);

	int x = 10;
	for (int i = 0; i < pPlayer->getHealth(); i++)
	{
		bmpHealth[i]->Draw(hdc, x, 10, TRUE);
		x += 15;
	}
}

void GameCycle()
{
	HWND hWindow = gGame->getWindow();
	HDC hdc = GetDC(hWindow);

	GamePaint(gOffsetScreenDC);

	BitBlt(hdc, 0,0, gGame->getWidth(), gGame->getHeight(), gOffsetScreenDC, 0,0, SRCCOPY);
	ReleaseDC(hWindow, hdc);
}


void GameCycleSecView()
{
	HWND hWindow = gGame->getWindow();
	HDC hdc = GetDC(hWindow);

	
	GamePaintSecView(gOffsetScreenDC);

	BitBlt(hdc, 0,0, gGame->getWidth(), gGame->getHeight(), gOffsetScreenDC, 0,0, SRCCOPY);
	ReleaseDC(hWindow, hdc);
}
