#include "..\includes\GameIntro.h"

void GameIntroInit(HWND hWindow, HINSTANCE ghInstance)
{
	srand(GetTickCount());

	gOffsetScrDC = CreateCompatibleDC(GetDC(hWindow));
	gOffsetScrBitmap = CreateCompatibleBitmap(GetDC(hWindow), 600, 400);
	SelectObject(gOffsetScrDC, gOffsetScrBitmap);

	HDC hdc = GetDC(hWindow);

	bmpBg = new Bitmap (hdc, IDB_INTRO, ghInstance);

	player = new Bitmap(hdc, IDB_PLAYER3LVL, ghInstance);

	for (int i = 0; i < 10; i++)
		array[i] = new Bitmap(hdc, IDB_PLAYER3LVL, ghInstance);

	xP = 0;
	arrayX = -40;
}

void drawIntro(HDC hdc)
{
	bmpBg->Draw(hdc, 0,0);
	int y = 0;
		int k = 0;

	if (xP <= 500)
		xP+= 10;

	else if (arrayX <= 250)
	{
		arrayX += 10;
	}
	else if (arrayX >= 250 && xP <= 600)
		xP += 10;
	else if (arrayX <= 600)
		arrayX += 10;
		
	if (xP >= 500)
	{
		k = 0;
		y = 220;
		for (int i = 0; i < 10; i++)
		{
			if (i ==5)
			{
				y += 40;
				k -= 200; 
			}
			array[i]->Draw(hdc, arrayX + k, y, TRUE);
			k += 40;
		}
	}
	player->Draw(hdc, xP,250, TRUE);
}

void GameIntroDraw(HWND hWindowM)
{
	HWND hWindow = hWindowM;
	HDC hdc = GetDC(hWindow);

	drawIntro(gOffsetScrDC);

	BitBlt(hdc, 0,0, 600, 400, gOffsetScrDC, 0,0, SRCCOPY);
	ReleaseDC(hWindow, hdc);
}

void IntroDel()
{
	DeleteObject(gOffsetScrBitmap);
	DeleteDC(gOffsetScrDC);

	delete bmpBg;
	delete player;

	for (int i = 0 ; i < 10; i++)
		delete array[i];

}