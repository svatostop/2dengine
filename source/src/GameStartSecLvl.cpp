#include "..\includes\Builder.h"

void GameStartSecLvl(HWND hWindow)
{
	srand(GetTickCount());

	gOffsetScreenDC = CreateCompatibleDC(GetDC(hWindow));
	gOffsetScreenBitmap = CreateCompatibleBitmap(GetDC(hWindow), gGame->getWidth(), gGame->getHeight());
	SelectObject(gOffsetScreenDC, gOffsetScreenBitmap);

	HDC hdc = GetDC(hWindow);

	checkLine = 3;
	bSecViewUpd = FALSE;
bSecViewUpdDown =  FALSE;
	gGalaxyBitmap = new Bitmap (hdc, IDB_BG2, ghInstance);

	bmpPlayer = new Bitmap (hdc, IDB_PLAYER2LVL, ghInstance);
	bmpPlayer2 = new Bitmap (hdc, IDB_PLAYER2LVL2, ghInstance);

	bmpFloor = new Bitmap(hdc, IDB_FLOOR2, ghInstance);
	bmpCell = new Bitmap (hdc, IDB_CELL, ghInstance);
	bmpCell2 = new Bitmap (hdc, IDB_WALL2, ghInstance);
	bmpCell3 = new Bitmap (hdc, IDB_WALL3, ghInstance);
	bmpSprite1 = new Bitmap (hdc, IDB_SPRITE4, ghInstance);
	bmpSprite2 = new Bitmap (hdc, IDB_SPRITE3, ghInstance);
	bmpCellSec = new Bitmap (hdc, IDB_CELLSEC, ghInstance);
	bmpDoor = new Bitmap (hdc, IDB_DOOR, ghInstance);

	bmpKey = new Bitmap (hdc, IDB_KEY, ghInstance);

	for (int i = 0; i < 3; i++)
	{
		bmpHealth[i] = new Bitmap(hdc, IDB_HEALTH, ghInstance);
	}
	
	Background *pBG;
	Sprite *pSpritesFirst;

	std::string strMap = gGame->getMap();

	std::istringstream map;
	map.str(strMap);
	std::string tmp;
	int xPosBmp = 0;
	int yPosBmp = 0;

	int yCounter = gGame->getYMapLen();

	int kof = yCounter * 40;

	if (kof < gGame->getHeight())
		yPosBmp = (gGame->getHeight() - kof)/2;

	while (std::getline(map, tmp))
	{
		xPosBmp = 0;
		for (int i = 0; tmp[i]; i++)
		{
			if (tmp[i] == '1')
			{
				pBG = new Background(bmpCell, {xPosBmp, yPosBmp}, TRUE);
				gGame->addBackground<Background, std::vector<Background*>>(pBG, gGame->getVbg());
		
			}
			if (tmp[i] == '.' || tmp[i] == '@')
			{
				if (tmp[i] == '@')
				{
					PlayerSide = RIGHT_SIDE;
					pPlayer = new Player(bmpPlayer, {xPosBmp, yPosBmp});
					pPlayerLeft = new Player(bmpPlayer2,{xPosBmp, yPosBmp});
				}
				pBG = new Background(bmpFloor, {xPosBmp, yPosBmp}, FALSE);
				gGame->addBackground<Background, std::vector<Background*>>(pBG, gGame->getVbg());
			}
			if (tmp[i] == '2')
			{
				pBG = new Background(bmpFloor, {xPosBmp, yPosBmp}, FALSE);
				gGame->addBackground<Background, std::vector<Background*>>(pBG, gGame->getVbg());
				pBG = new Background(bmpCell2, {xPosBmp, yPosBmp}, FALSE);
				gGame->addBackground<Background, std::vector<Background*>>(pBG, gGame->getVPlatforms());
			}
			if (tmp[i] == '3')
			{
				pBG = new Background(bmpFloor, {xPosBmp, yPosBmp}, FALSE);
				gGame->addBackground<Background, std::vector<Background*>>(pBG, gGame->getVbg());
				pBG = new Background(bmpCell3, {xPosBmp, yPosBmp}, FALSE);
				gGame->addBackground<Background, std::vector<Background*>>(pBG, gGame->getVPlatforms());
			}
			if (tmp[i] == '*')
			{
				pBG = new Background(bmpFloor, {xPosBmp, yPosBmp}, FALSE);
				gGame->addBackground<Background, std::vector<Background*>>(pBG, gGame->getVbg());
				pSpritesFirst = new Sprite(bmpSprite1, {xPosBmp, yPosBmp}, TRUE, ON_TEXT);
				gGame->addBackground<Sprite, std::vector<Sprite*>>(pSpritesFirst, gGame->getVFirstSprite());
			}
			if (tmp[i] == 'L')
			{
				pBG = new Background(bmpFloor, {xPosBmp, yPosBmp}, FALSE);
				gGame->addBackground<Background, std::vector<Background*>>(pBG, gGame->getVbg());
				pSpritesFirst = new Sprite(bmpDoor, {xPosBmp, yPosBmp}, TRUE, NEXT_LVL_KEY);
				gGame->addBackground<Sprite, std::vector<Sprite*>>(pSpritesFirst, gGame->getVFirstSprite());
			}
			if (tmp[i] == 'o')
			{
				pBG = new Background(bmpFloor, {xPosBmp, yPosBmp}, FALSE);
				gGame->addBackground<Background, std::vector<Background*>>(pBG, gGame->getVbg());
				pSpritesFirst = new Sprite(bmpSprite2, {xPosBmp, yPosBmp}, TRUE, ENEMY);
				gGame->addBackground<Sprite, std::vector<Sprite*>>(pSpritesFirst, gGame->getEnemySprite());
			}
			xPosBmp += 40;
		}
		yPosBmp += 40;
	}

	std::map<int, std::vector<std::string>> viewMap = gGame->getMView();

	yPosBmp = gGame->getHeight() - 100;
	xPosBmp = 0;

	Background *secViewBG;
	Sprite *pSprites;

	std::vector<std::string> firS;

	for (int o = 0; o < yCounter; o++)
	{
		firS = viewMap[o];
		yPosBmp = gGame->getHeight() - 40;
		for (int y = 0; y < firS.size(); y++)
		{
			xPosBmp = 0;

			for (int i = 0; firS[y][i]; i++)
			{
				if (o == 0 && y == 0 && firS[y][i] == '1')
				{
					pSprites = new Sprite(bmpFloor, {xPosBmp, yPosBmp}, TRUE, OFF);
					gGame->addBackground<Sprite, std::vector<Sprite*>>(pSprites, gGame->getVSprite());
					pSprites = new Sprite(bmpFloor, {xPosBmp, yPosBmp - 40}, TRUE, OFF);
					gGame->addBackground<Sprite, std::vector<Sprite*>>(pSprites, gGame->getVSprite());
					secViewBG = new Background(bmpCellSec, {xPosBmp,  yPosBmp - 80}, FALSE);
					gGame->addBackground<Background, std::vector<Background*>>(secViewBG, gGame->getSecVbg());
					secViewBG = new Background(bmpCellSec, {xPosBmp,  yPosBmp - 120}, FALSE);
					gGame->addBackground<Background, std::vector<Background*>>(secViewBG, gGame->getSecVbg());
				}
				if (y == 0 && firS[y][i] == ' ')
				{
				 	if (firS[y][i] == ' ')
				 		xPosBmp += 40;

				}
				if (y == 1 && firS[y][i] == '2')
				{
					pSprites = new Sprite(bmpCell2, {xPosBmp, yPosBmp - 120}, TRUE, OFF_TEXT);
					gGame->addBackground<Sprite, std::vector<Sprite*>>(pSprites, gGame->getVSprite());
				}
				if (y == 2 && firS[y][i] == '3')
				{
					pSprites = new Sprite(bmpCell3, {xPosBmp, yPosBmp - 160},TRUE, OFF_TEXT);
					gGame->addBackground<Sprite, std::vector<Sprite*>>(pSprites, gGame->getVSprite());
				}
				if (y == 3 && (firS[y][i] == '4' || firS[y][i] == '-'))
				{
					if (firS[y][i] == '-')
					{
						pSpritesFirst = new Sprite(bmpSprite2, {xPosBmp, yPosBmp - 240}, TRUE, HIDE_PLAT);
						gGame->addBackground<Sprite, std::vector<Sprite*>>(pSpritesFirst, gGame->getEnemySpriteSec());
					}
					pSprites = new Sprite(bmpCell2, {xPosBmp, yPosBmp - 200},TRUE, OFF_TEXT);
					gGame->addBackground<Sprite, std::vector<Sprite*>>(pSprites, gGame->getVSprite());
				}
				if (y == 4 && (firS[y][i] == '5' || firS[y][i] == '-'))
				{
					if (firS[y][i] == '-')
					{
						pSpritesFirst = new Sprite(bmpSprite2, {xPosBmp, yPosBmp - 320}, TRUE, HIDE_PLAT);
						gGame->addBackground<Sprite, std::vector<Sprite*>>(pSpritesFirst, gGame->getEnemySpriteSec());
					}
					pSprites = new Sprite(bmpCell3, {xPosBmp, yPosBmp - 280},TRUE, OFF_TEXT);
					gGame->addBackground<Sprite, std::vector<Sprite*>>(pSprites, gGame->getVSprite());
				}
				if (y == 5 && (firS[y][i] == '6' || firS[y][i] == '!'))
				{
					if (firS[y][i] == '!')
					{
						pSprites = new Sprite(bmpKey, {xPosBmp, yPosBmp - 400}, TRUE, KEY);
						gGame->addBackground<Sprite, std::vector<Sprite*>>(pSprites, gGame->getVSprite());
					}
					pSprites = new Sprite(bmpCell2, {xPosBmp, yPosBmp - 360},TRUE, OFF_TEXT);
					gGame->addBackground<Sprite, std::vector<Sprite*>>(pSprites, gGame->getVSprite());
				}
				if (firS[y][i] == '*' )
				{
					pSprites = new Sprite(bmpSprite1, {xPosBmp,  yPosBmp - 100}, FALSE, OFF_TEXT);
					gGame->addBackground<Sprite, std::vector<Sprite*>>(pSprites, gGame->getVSprite());
				}
				if (firS[y][i] == 'L')
				{
					pSprites = new Sprite(bmpDoor, {xPosBmp,  yPosBmp - 100}, FALSE, OFF_TEXT);
					gGame->addBackground<Sprite, std::vector<Sprite*>>(pSprites, gGame->getVSprite());
				}
				xPosBmp += 40;
			}
		}
	}

	pPlayer->setJump(FALSE);
	pPlayer->setJumpEnd(FALSE);
	pPlayer->setJumpScaleEnd(gGame->getHeight() - 140);
	pPlayerLeft->setJump(FALSE);
	pPlayerLeft->setJumpEnd(FALSE);
	pPlayerLeft->setJumpScaleEnd(gGame->getHeight() - 140);
	initTexts();

}