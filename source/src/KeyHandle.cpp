#include "..\includes\Builder.h"

 HINSTANCE ghInstance;
 GameEngine *gGame;
 Bitmap *gGalaxyBitmap;
 Bitmap *bmpFloor;
 Bitmap *bmpCellSec;
 Bitmap *bmpCell;
 Bitmap *bmpCell2;
 Bitmap *bmpCell3;
 Bitmap *bmpPlayer;
 Bitmap *bmpPlayer2;
 Bitmap *bmpSprite1;
 Bitmap *bmpSprite2;
 Bitmap *bmpDoor;
Bitmap *bmpKey; 
 Bitmap *bmpHealth[3];
 Player *pPlayer;
 Player *pPlayerLeft;
 RECT saveViewLoc;
 RECT saveViewLocLeft;

 int PlayerSide;

 int secViewOffset;
 BOOL flagOffset;

 BOOL bGameViewDef;

 HDC gOffsetScreenDC;
 HBITMAP gOffsetScreenBitmap;
 BOOL bSecViewUpd;
BOOL bSecViewUpdDown;


int checkLine;

void HandleUp(Player *CurPl)
{
	RECT checkBoundsRect = gGame->getWinBounds();
    RECT oldPos =  CurPl->getPosition();
	RECT checkPos = CurPl->getPosition();
	
	checkPos.top -= 10;

	CurPl->Update(0, -10);

	if (checkPos.top < checkBoundsRect.top)
	{
		if ((gGame->checkBounds(CurPl, oldPos) == FALSE)
				&& (gGame->checkBoundsSprite<std::vector<Sprite*>>(CurPl, oldPos, gGame->getVFirstSprite()) == FALSE)
				&& (gGame->checkBoundsSprite<std::vector<Sprite*>>(CurPl, oldPos, gGame->getEnemySprite()) == FALSE))
		{
			CurPl->Update(0, -10);

			gGame->updateBgFrame(0, 10, CurPl, oldPos);
			RECT newPos =  CurPl->getPosition();

			gGame->updateSpriteFrame<std::vector<Sprite*>>(0, 10, CurPl, newPos, gGame->getVFirstSprite());
			gGame->updateSpriteFrame<std::vector<Sprite*>>(0, 10, CurPl, newPos, gGame->getEnemySprite());
			gGame->updateSpriteFrame<std::vector<Sprite*>>(0, 10, CurPl, newPos, gGame->getEnemySpriteSec());

			gGame->updateSecView<Background, std::vector<Background*>>(0, 10, gGame->getVPlatforms());
		}
	}
	else
	{
		gGame->checkBounds(CurPl, oldPos);
		gGame->checkBoundsSprite<std::vector<Sprite*>>(CurPl, oldPos, gGame->getVFirstSprite());
		gGame->checkBoundsSprite<std::vector<Sprite*>>(CurPl, oldPos, gGame->getEnemySprite());
	}
}

void HandleDown(Player *CurPl)
{
	RECT checkBoundsRect = gGame->getWinBounds();
    RECT oldPos =  CurPl->getPosition();
	RECT checkPos = CurPl->getPosition();
	
	checkPos.top += 10;

	CurPl->Update(0, 10);

	if (checkPos.top > checkBoundsRect.bottom)
	{
		if ((gGame->checkBounds(CurPl, oldPos) == FALSE)
				&& (gGame->checkBoundsSprite<std::vector<Sprite*>>(CurPl, oldPos, gGame->getVFirstSprite()) == FALSE)
				&& (gGame->checkBoundsSprite<std::vector<Sprite*>>(CurPl, oldPos, gGame->getEnemySprite()) == FALSE))
		{
			CurPl->Update(0, 10);

			gGame->updateBgFrame(0, -10, CurPl, oldPos);
			RECT newPos =  CurPl->getPosition();

			gGame->updateSpriteFrame<std::vector<Sprite*>>(0, -10, CurPl, newPos, gGame->getVFirstSprite());
			gGame->updateSpriteFrame<std::vector<Sprite*>>(0, -10, CurPl, newPos, gGame->getEnemySprite());
			gGame->updateSpriteFrame<std::vector<Sprite*>>(0, -10, CurPl, newPos, gGame->getEnemySpriteSec());

			gGame->updateSecView<Background, std::vector<Background*>>(0, -10, gGame->getVPlatforms());
		}
	}
	else
	{
		gGame->checkBounds(CurPl, oldPos);
		gGame->checkBoundsSprite<std::vector<Sprite*>>(CurPl, oldPos, gGame->getVFirstSprite());
		gGame->checkBoundsSprite<std::vector<Sprite*>>(CurPl, oldPos, gGame->getEnemySprite());
	}
}

void HandleLeft(Player *CurPl)
{
	RECT oldPos =  CurPl->getPosition();
	RECT checkBoundsRect = gGame->getWinBounds();
	RECT checkPos = CurPl->getPosition();

	checkPos.left -= 10;

	CurPl->Update(-10, 0);

	if (checkPos.left < checkBoundsRect.left)
	{
		if ((gGame->checkBounds(CurPl, oldPos) == FALSE)
			&& (gGame->checkBoundsSprite<std::vector<Sprite*>>(CurPl, oldPos, gGame->getVFirstSprite()) == FALSE)
			&& (gGame->checkBoundsSprite<std::vector<Sprite*>>(CurPl, oldPos, gGame->getEnemySprite()) == FALSE))
		{
			CurPl->Update(-10, 0);

			gGame->updateBgFrame(10, 0, CurPl, oldPos);
			RECT newPos =  CurPl->getPosition();

			gGame->updateSpriteFrame<std::vector<Sprite*>>(10, 0, CurPl, newPos, gGame->getVFirstSprite());
			gGame->updateSpriteFrame<std::vector<Sprite*>>(10, 0, CurPl, newPos, gGame->getEnemySprite());
			gGame->updateSpriteFrame<std::vector<Sprite*>>(10, 0, CurPl, newPos, gGame->getEnemySpriteSec());

			gGame->updateSecView<Background, std::vector<Background*>>(10, 0, gGame->getVPlatforms());

				
			gGame->updateSecView<Background, std::vector<Background*>>(10, 0, gGame->getSecVbg());
			gGame->updateSecView<Sprite, std::vector<Sprite*>>(10, 0, gGame->getVSprite());
		}
	}
	else
	{
		gGame->checkBounds(CurPl, oldPos);
		gGame->checkBoundsSprite<std::vector<Sprite*>>(CurPl, oldPos, gGame->getVFirstSprite());
		gGame->checkBoundsSprite<std::vector<Sprite*>>(CurPl, oldPos, gGame->getEnemySprite());
	}
}

void HandleRight(Player *CurPl)
{
	RECT oldPos =  CurPl->getPosition();
	RECT checkBoundsRect = gGame->getWinBounds();
	RECT checkPos = CurPl->getPosition();

	checkPos.left += 10;
	CurPl->Update(10, 0);

	if (checkPos.left > checkBoundsRect.right)
	{
		if ((gGame->checkBounds(CurPl, oldPos) == FALSE)
			&& (gGame->checkBoundsSprite<std::vector<Sprite*>>(CurPl, oldPos, gGame->getVFirstSprite()) == FALSE)
			&& (gGame->checkBoundsSprite<std::vector<Sprite*>>(CurPl, oldPos, gGame->getEnemySprite()) == FALSE))
		{
			CurPl->Update(10, 0);

			gGame->updateBgFrame(-10, 0, CurPl, oldPos);
			RECT newPos =  CurPl->getPosition();

			gGame->updateSpriteFrame<std::vector<Sprite*>>(-10, 0, CurPl, newPos, gGame->getVFirstSprite());
			gGame->updateSpriteFrame<std::vector<Sprite*>>(-10, 0, CurPl, newPos, gGame->getEnemySprite());
			gGame->updateSpriteFrame<std::vector<Sprite*>>(-10, 0, CurPl, newPos, gGame->getEnemySpriteSec());

			gGame->updateSecView<Background, std::vector<Background*>>(-10, 0, gGame->getVPlatforms());

				
			gGame->updateSecView<Background, std::vector<Background*>>(-10, 0, gGame->getSecVbg());
			gGame->updateSecView<Sprite, std::vector<Sprite*>>(-10, 0, gGame->getVSprite());
		}
	}
	else
	{
		gGame->checkBounds(CurPl, oldPos);
		gGame->checkBoundsSprite<std::vector<Sprite*>>(CurPl, oldPos, gGame->getVFirstSprite());		
		gGame->checkBoundsSprite<std::vector<Sprite*>>(CurPl, oldPos, gGame->getEnemySprite());		

	}
}

void HandleKeys()
{
	HBRUSH blBr = CreateSolidBrush(RGB(0,0,0));
	RECT rc = {0,0, gGame->getWidth(), gGame->getHeight()};
	FillRect(gOffsetScreenDC, &rc, blBr);

    if (GetAsyncKeyState(E_KEY) < 0)
    {
    	RECT oldPos =  pPlayer->getPosition();

    	saveViewLoc = pPlayer->getPosition();

    	pPlayer->setPosition({oldPos.left, gGame->getHeight()-140});
    	pPlayerLeft->setPosition({oldPos.left, gGame->getHeight()-140});

    	gGame->updateSecViewSprite(1);
		gGame->updateSecViewSpriteEnemy(1);
    	gGame->setGameState(GAME_STATE_RUN_SECVIEW);
    	return ;
    }
	if (GetAsyncKeyState(A_KEY) < 0)
	{
		PlayerSide = LEFT_SIDE;

		HandleLeft(pPlayerLeft);
		
		RECT ppos = pPlayerLeft->getPosition();
		pPlayer->setPosition({ppos.left, ppos.top});

	}
	else if (GetAsyncKeyState(D_KEY) < 0)
	{
		PlayerSide = RIGHT_SIDE;
		
		HandleRight(pPlayer);
		
		RECT ppos = pPlayer->getPosition();
		pPlayerLeft->setPosition({ppos.left, ppos.top});

	}
	if (GetAsyncKeyState(S_KEY) < 0)
	{
		if (PlayerSide == RIGHT_SIDE)
		{
			HandleDown(pPlayer);

			RECT ppos = pPlayer->getPosition();
			pPlayerLeft->setPosition({ppos.left, ppos.top});
		}
		if (PlayerSide == LEFT_SIDE)
		{
			HandleDown(pPlayerLeft);

			RECT ppos = pPlayerLeft->getPosition();
			pPlayer->setPosition({ppos.left, ppos.top});
		}

	}
	else if (GetAsyncKeyState(W_KEY) < 0)
	{
		if (PlayerSide == RIGHT_SIDE)
		{
			HandleUp(pPlayer);
	
			RECT ppos = pPlayer->getPosition();
			pPlayerLeft->setPosition({ppos.left, ppos.top});
		}
		if (PlayerSide == LEFT_SIDE)
		{
			HandleUp(pPlayerLeft);

			RECT ppos = pPlayerLeft->getPosition();
			pPlayer->setPosition({ppos.left, ppos.top});
		}
	}
	if (PlayerSide == LEFT_SIDE)
	{
		RECT oldPos =  pPlayerLeft->getPosition();
		gGame->checkBoundsSprite<std::vector<Sprite*>>(pPlayerLeft, oldPos, gGame->getEnemySprite());

	}
	else
	{
		RECT oldPos =  pPlayer->getPosition();
		gGame->checkBoundsSprite<std::vector<Sprite*>>(pPlayer, oldPos, gGame->getEnemySprite());

	}
}