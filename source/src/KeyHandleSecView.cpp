#include "..\includes\Builder.h"

void HandleJumpEvents(Player *CurPl)
{
	RECT oldPos =  CurPl->getPosition();
	RECT checkBounds = gGame->getWinBounds();

	if (CurPl->jumpEvent() == TRUE)
    {
		RECT checkPos = CurPl->getPosition();

      	CurPl->JumpUpdate(0, -10);
		CurPl->setJumpStart(TRUE);


		if (bSecViewUpd == FALSE && (checkPos.top == 50))
		{
			bSecViewUpd = TRUE;
			gGame->updateBgFrSec(0, 50, CurPl, oldPos);
			RECT newPos =  CurPl->getPosition();

			gGame->updateSpriteFrame<std::vector<Sprite*>>(0, 50, CurPl, newPos, gGame->getVSprite());
			gGame->updateSpriteFrame<std::vector<Sprite*>>(0, 50, CurPl, newPos, gGame->getEnemySprite());
			gGame->updateSpriteFrame<std::vector<Sprite*>>(0, 50, CurPl, newPos, gGame->getEnemySpriteSec());

			bSecViewUpdDown =  TRUE;
			if (gGame->checkBoundsSprite<std::vector<Sprite*>>(CurPl, checkPos, gGame->getVSprite()) != FALSE) 
			{
				CurPl->setJump(FALSE);
				CurPl->setJumpEnd(TRUE);
	      	CurPl->JumpUpdate(0, 30);
	      	gGame->setJmpKey(TRUE);
				HandleJumpEvents(CurPl);
			}
		}

    	if (gGame->checkBoundsSprite<std::vector<Sprite*>>(CurPl, oldPos, gGame->getVSprite()) == TRUE)
    	{
    		CurPl->setJump(FALSE);
			CurPl->setJumpEnd(TRUE);

			HandleJumpEvents(CurPl);
    	}
    }
    else if (CurPl->jumpEventEnd() == TRUE)
    {
		RECT checkPos = CurPl->getPosition();

      	CurPl->JumpUpdate(0, 10);
      	gGame->checkEnemyBounds(CurPl, oldPos);
      	gGame->checkEnemyBoundsSec(CurPl, oldPos);

      	if (checkPos.top >  gGame->getHeight())
      		GameEngine::getEngine()->setGameState(GAME_STATE_END);

    	if (checkPos.top == gGame->getHeight() - 140 && bSecViewUpdDown ==  TRUE)
		{
			bSecViewUpdDown =  FALSE;

			gGame->updateBgFrSec(0, -50, CurPl, oldPos);
			RECT newPos =  CurPl->getPosition();

			gGame->updateSpriteFrame<std::vector<Sprite*>>(0, -50, CurPl, newPos, gGame->getVSprite());
			gGame->updateSpriteFrame<std::vector<Sprite*>>(0,-50, CurPl, newPos, gGame->getEnemySprite());
			gGame->updateSpriteFrame<std::vector<Sprite*>>(0, -50, CurPl, newPos, gGame->getEnemySpriteSec());

			bSecViewUpd = FALSE;

		}

    	if (gGame->checkBoundsSprite<std::vector<Sprite*>>(CurPl, oldPos, gGame->getVSprite()) == TRUE)
    	{
			CurPl->setJumpEnd(FALSE);
			CurPl->setJumpStart(FALSE);
    	}
		

    }
}

void HandleRightSide()
{
	HandleJumpEvents(pPlayer);
	RECT ppos = pPlayer->getPosition();
	pPlayerLeft->setPosition({ppos.left, ppos.top});
	pPlayerLeft->setJump(pPlayer->jumpEvent()) ;
	pPlayerLeft->setJumpEnd( pPlayer->jumpEventEnd());
	pPlayerLeft->setJumpStart(pPlayer->getJumpStart());
}

void HandleLeftSide()
{
	HandleJumpEvents(pPlayerLeft);
	RECT ppos = pPlayerLeft->getPosition();
	pPlayer->setPosition({ppos.left, ppos.top});
	pPlayer->setJump(pPlayerLeft->jumpEvent());
	pPlayer->setJumpEnd(pPlayerLeft->jumpEventEnd());
	pPlayer->setJumpStart(pPlayerLeft->getJumpStart());
}

void HandleJumpKeys()
{
	RECT checkBounds = gGame->getWinBounds();

	if (PlayerSide == RIGHT_SIDE)
	{
		HandleRightSide();
	}
	if (PlayerSide == LEFT_SIDE)
	{
		HandleLeftSide();
	}
    if (GetAsyncKeyState(A_KEY) < 0)
	{
		PlayerSide = LEFT_SIDE;

    	RECT oldPos =  pPlayerLeft->getPosition();

		if (oldPos.left > checkBounds.left - 100)
			pPlayerLeft->Update(-10, 0);
		
    	if (gGame->checkBoundsSprite<std::vector<Sprite*>>(pPlayerLeft, oldPos, gGame->getVSprite()) == TRUE)
    	{
			pPlayerLeft->setJump(FALSE);
			pPlayerLeft->setJumpEnd(TRUE);
			HandleLeftSide();
    	}
    	RECT ppos = pPlayerLeft->getPosition();
		pPlayer->setPosition({ppos.left, ppos.top});
	}
	else if (GetAsyncKeyState(D_KEY) < 0)
	{
		PlayerSide = RIGHT_SIDE;

    	RECT oldPos =  pPlayer->getPosition();

		if (oldPos.left < checkBounds.right + 250)
			pPlayer->Update(10, 0);
		
    	if (gGame->checkBoundsSprite<std::vector<Sprite*>>(pPlayer, oldPos, gGame->getVSprite()) == TRUE)
    	{
			pPlayer->setJump(FALSE);
			pPlayer->setJumpEnd(TRUE);
			HandleRightSide();
    	}
    	RECT ppos = pPlayer->getPosition();
		pPlayerLeft->setPosition({ppos.left, ppos.top});
	}
}

void HandleKeysSecView()
{
	if (pPlayer->jumpEvent() == TRUE || pPlayer->jumpEventEnd() == TRUE)
	{
		HandleJumpKeys();
		return ;
	}
	else if (pPlayerLeft->jumpEvent() == TRUE || pPlayerLeft->jumpEventEnd() == TRUE)
	{
		HandleJumpKeys();
		return ;
	}
   	if (GetAsyncKeyState(F_KEY) < 0 && bSecViewUpdDown == FALSE)
    {
		RECT oldPos =  pPlayer->getPosition();
		RECT oldPosLeft = pPlayerLeft->getPosition();

    	if (PlayerSide == RIGHT_SIDE)
    	{
    		pPlayer->setPosition({oldPos.left, saveViewLoc.top});

			if (gGame->checkBounds(pPlayer, oldPos) == FALSE
				&& gGame->checkBoundsSprite<std::vector<Sprite*>>(pPlayer, oldPos,gGame->getVFirstSprite()) == FALSE)
			{
				gGame->updateSecViewSprite(0);
				gGame->updateSecViewSpriteEnemy(0);

				gGame->setGameState(GAME_STATE_RUN);
				RECT ppos = pPlayer->getPosition();
				pPlayerLeft->setPosition({ppos.left, ppos.top});	
				return ;
			}
		}
		else if (PlayerSide == LEFT_SIDE)
    	{

    		pPlayerLeft->setPosition({oldPosLeft.left, saveViewLoc.top});

			if (gGame->checkBounds(pPlayerLeft, oldPos)== FALSE
				&& gGame->checkBoundsSprite<std::vector<Sprite*>>(pPlayerLeft, oldPosLeft, gGame->getVFirstSprite()) == FALSE)
			{
				gGame->updateSecViewSprite(0);
				gGame->updateSecViewSpriteEnemy(0);

				gGame->setGameState(GAME_STATE_RUN);
				RECT ppos = pPlayerLeft->getPosition();
				pPlayer->setPosition({ppos.left, ppos.top});	
				return ;
			}
		}
	}
    if (GetAsyncKeyState(VK_SPACE) < 0 && ((pPlayerLeft->jumpEvent() == FALSE && pPlayerLeft->jumpEventEnd() == FALSE)
    	|| (pPlayer->jumpEvent() == FALSE && pPlayer->jumpEventEnd() == FALSE)))
    {
		RECT oldPos =  pPlayer->getPosition();

    	pPlayerLeft->setJump(TRUE);
    	pPlayerLeft->setJumpScale(oldPos.top - 80);
    	pPlayer->setJump(TRUE);
    	pPlayer->setJumpScale(oldPos.top - 80);
    }
	if (GetAsyncKeyState(A_KEY) < 0)
	{
		PlayerSide = LEFT_SIDE;

		RECT oldPos =  pPlayerLeft->getPosition();
		RECT checkBounds = gGame->getWinBounds();

		if (oldPos.left > checkBounds.left - 100)
		{
		    pPlayerLeft->Update(-10, 0);
			
			if ((gGame->checkEnemyBounds(pPlayerLeft, oldPos) == FALSE)
				&& gGame->checkEnemyBoundsSec(pPlayerLeft, oldPos) == FALSE)
			{
				RECT checkPos = pPlayerLeft->getPosition();
		    	if (checkPos.top < (pPlayerLeft->getJumpScEnd()))
		    	{
					pPlayer->setJumpStart(TRUE);
					pPlayerLeft->setJumpStart(TRUE);

		    		pPlayerLeft->setJumpEnd(TRUE);
		    		pPlayer->setJumpEnd(TRUE);
		    	}
		    	oldPos =  pPlayerLeft->getPosition();

		    	  if (gGame->checkBoundsSprite<std::vector<Sprite*>>(pPlayerLeft, oldPos, gGame->getVSprite()) == FALSE)
		    	{
					pPlayer->setJumpStart(TRUE);
					pPlayerLeft->setJumpStart(TRUE);

		    		pPlayerLeft->setJumpEnd(TRUE);
		    		pPlayer->setJumpEnd(TRUE);
		    	}
	    	}
		}
		RECT ppos = pPlayerLeft->getPosition();
		pPlayer->setPosition({ppos.left, ppos.top});	
	}
	else if (GetAsyncKeyState(D_KEY) < 0)
	{
		PlayerSide = RIGHT_SIDE;

		RECT oldPos =  pPlayer->getPosition();
		RECT checkBounds = gGame->getWinBounds();

		if (oldPos.left < checkBounds.right + 250)
		{
		    pPlayer->Update(10, 0);
			
			if ((gGame->checkEnemyBounds(pPlayer, oldPos) == FALSE)
				&& gGame->checkEnemyBoundsSec(pPlayer, oldPos) == FALSE)
			{
				RECT checkPos = pPlayer->getPosition();
		    	if (checkPos.top < (pPlayer->getJumpScEnd()))
		    	{
					pPlayer->setJumpStart(TRUE);
					pPlayerLeft->setJumpStart(TRUE);

		    		pPlayer->setJumpEnd(TRUE);
		    		pPlayerLeft->setJumpEnd(TRUE);
		    	}
		    	oldPos =  pPlayer->getPosition();
		    	if (gGame->checkBoundsSprite<std::vector<Sprite*>>(pPlayer, oldPos, gGame->getVSprite()) == FALSE)
		    	{
					pPlayer->setJumpStart(TRUE);
					pPlayerLeft->setJumpStart(TRUE);

		    		pPlayer->setJumpEnd(TRUE);
		    		pPlayerLeft->setJumpEnd(TRUE);
		    	}
	    	}
	    	
		}
		RECT ppos = pPlayer->getPosition();
		pPlayerLeft->setPosition({ppos.left, ppos.top});	
	}
	// else
	// {
		if (PlayerSide == LEFT_SIDE)
		{
			RECT oldPos =  pPlayerLeft->getPosition();
			gGame->checkEnemyBounds(pPlayerLeft, oldPos);
			gGame->checkEnemyBoundsSec(pPlayerLeft, oldPos);
		}
		else
		{
			RECT oldPos =  pPlayer->getPosition();
			gGame->checkEnemyBounds(pPlayer, oldPos);
			gGame->checkEnemyBoundsSec(pPlayerLeft, oldPos);

		}
//	}
}
