#include "..\includes\ParseParams.h"

mainParams::mainParams()
{
	w=0;
	h=0;
	mode=0;
	map = "";
	mapFirstLvl = "";
	mapSecondLvl = "";
	str = "";
	yLen = 0;
}

void mainParams::setRes()
{
	std::size_t find = this->str.find("RES:");
	if (find == std::string::npos)
	{
		char err[40] = "Error: <RES:> not found in level-file!";
		printError(err);
	}

	std::string numbers = " 0123456789";
	std::string finRes = "";

	std::size_t found = this->str.find_first_of(numbers.c_str());

	if (found == std::string::npos)
	{
		char err[40] = "Error: <RES:> not found in level-file!";
		printError(err);
	}
	int i = found;
	while ( this->str[i] >= '0' && this->str[i] <= '9'
		|| this->str[i] == ' ')
	{
		finRes += this->str[i];
		i++;
	}
	std::stringstream ss;
	ss << finRes;
	ss >> this->w;
	ss >> this->h;

	finRes.clear();
	ss.str("");

	if (this->w < 300 || this->w > 1280)
	{
		char err[40] = "Error: <RES: Width> is not correct!";

		printError(err);
	}
	if (this->h < 300 || this->h > 1024)
	{
		char err[40] = "Error: <RES: Height> is not correct!";
		printError(err);
	}
}
void mainParams::setMap()
{
	std::size_t find = this->str.find("MAP:");
	if (find == std::string::npos)
	{
		char err[40] = "Error: <MAP:> not found in level-file!";
		printError(err);
	}
	find = this->str.find("%0");
	if (find == std::string::npos)
	{
		char err[45] = "Error: <MAP:> Can't find <%0> map!";
		printError(err);
	}

	std::string tmp = "";
	tmp = this->str.substr(find + 2);
	int i = 0;

	find = tmp.find_first_of("1");

	if (find == std::string::npos)
	{
		char err[40] = "Error: <MAP:> Can't find bounds in map!";
		printError(err);
	}

	tmp.erase(tmp.begin(), tmp.begin() + find);

	find = tmp.find("%1");

	if (find == std::string::npos)
	{
		char err[45] = "Error: <MAP:> Can't find <%1> map!";
		printError(err);
	}
	this->map.append(tmp.begin(), tmp.begin() + find - 1);

	tmp.erase(tmp.begin(), tmp.begin() + find + 2);
	find = tmp.find("%2");

	if (find == std::string::npos)
	{
		char err[45] = "Error: <MAP:> Can't find <%2> map!";
		printError(err);
	}
	this->mapFirstLvl.append(tmp.begin() + 2, tmp.begin() + find - 1);
	tmp.erase(tmp.begin(), tmp.begin() + find + 2);

	find = tmp.find("%3");

	if (find != std::string::npos)
	{
		this->mapSecondLvl.append(tmp.begin() +2, tmp.begin() + find - 1);
		tmp.erase(tmp.begin(), tmp.begin() + find + 2);

		find = tmp.find("%4");

		this->map3Lvl.append(tmp.begin() + 2, tmp.begin() + find - 1);

		tmp.erase(tmp.begin(), tmp.begin() + find + 2);

		find = tmp.find("%5");

		this->map4Lvl.append(tmp.begin() + 2, tmp.begin() + find - 1);
		tmp.erase(tmp.begin(), tmp.begin() + find + 2);

		find = tmp.find_first_of("%");

		if (find == std::string::npos)
		{
			char err[50] = "Error: <MAP:> Can't find <%> at the end of map!";
			printError(err);
		}
		this->map5Lvl.append(tmp.begin() + 2, tmp.begin() + find - 1);

	}
	else
	{
	
		find = tmp.find_first_of("%");

		if (find == std::string::npos)
		{
			char err[50] = "Error: <MAP:> Can't find <%> at the end of map!";
			printError(err);
		}

		this->mapSecondLvl.append(tmp.begin() +2, tmp.begin() + find - 1);
	}
	std::istringstream mapSs;

	mapSs.str(this->map);

	std::string tmp2;
	while (std::getline(mapSs, tmp2))
	{
		this->yLen++;
	}
	mapSs.str("");
	tmp2.clear();
	tmp.clear();
}

void mainParams::setMapView()
{
	std::stringstream t1, t2, t3, t4, t5, t6;

	t1.str(this->map);
	std::string mm;
	int k = 0;
	while (std::getline(t1, mm))
	{
		this->parseMap[k].push_back(mm);
		k++;
	}
	t1.str(std::string());
	t2.str(this->mapFirstLvl);
	std::string mm1;
	k = 0;
	while (std::getline(t2, mm1))
	{
		this->parseMap[k].push_back(mm1);
		k++;
	}
	t2.str(std::string());

	t3.str(this->mapSecondLvl);
	std::string mm2;
	k = 0;
	while (std::getline(t3, mm2))
	{
		this->parseMap[k].push_back(mm2);
		k++;
	}
	t3.str(std::string());

	if (this->map3Lvl != "")
	{
		t4.str(this->map3Lvl);
		std::string mm3;
		k = 0;
		while (std::getline(t4, mm3))
		{
			this->parseMap[k].push_back(mm3);
			k++;
		}
		t4.str(std::string());

		t5.str(this->map4Lvl);
		std::string mm4;
		k = 0;
		while (std::getline(t5, mm4))
		{
			this->parseMap[k].push_back(mm4);
			k++;
		}
		t5.str(std::string());

		t6.str(this->map5Lvl);
		std::string mm5;
		k = 0;
		while (std::getline(t6, mm5))
		{
			this->parseMap[k].push_back(mm5);
			k++;
		}
		t6.str(std::string());
	}
}
