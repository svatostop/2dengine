#include "..\includes\Player.h"

int Player::mHealth = 3;

Player::Player(Bitmap *pBitmap, POINT ptPosition)
{
	mBitmap = pBitmap;
	mJumpStart = FALSE;
	mKey = FALSE;
	SetRect(&mrPosition, ptPosition.x, ptPosition.y, ptPosition.x + pBitmap->getWidth(), ptPosition.y + pBitmap->getHeight());
	CalcCollisionRect();
}

Player::~Player()
{
}

void Player::Draw(HDC hdc)
{
	if (mBitmap != NULL)
		mBitmap->Draw(hdc, mrPosition.left, mrPosition.top, TRUE);
}
void Player::JumpUpdate(int x, int y)
{
	POINT newPos;

	if (mJumpActBegin == TRUE)
	{
		if (mrPosition.top + y >= mJumpScale)
		{
    		mJumpActEnd = FALSE;
			newPos.x = mrPosition.left + x;
			newPos.y = mrPosition.top + y;
			setPosition(newPos);
			return ;
		}
		else
		{
			mJumpActBegin = FALSE;
			mJumpActEnd = TRUE;
		}
	}
	if (mJumpActEnd == TRUE)
	{
		// if (mrPosition.top + y <= mJumpScaleEnd)
		// {
			newPos.x = mrPosition.left + x;
			newPos.y = mrPosition.top + y;
			setPosition(newPos);
		// 	mJumpActEnd = FALSE;
		
			return ;
		//}
		// else
		// 	mJumpActEnd = FALSE;
	}
}

void Player::Update(int x, int y)
{
	POINT newPos;
	newPos.x = mrPosition.left + x;
	newPos.y = mrPosition.top + y;

	setPosition(newPos);
}