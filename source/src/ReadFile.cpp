#include <iostream>
#include <windows.h>
#include <string>
#include <fstream>
#include <io.h>
#include <fcntl.h>

#include <algorithm>

//char *argv = NULL;

void printDbg(const char * res)
{
	AllocConsole();

	HANDLE stdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	if (stdOut != NULL && stdOut != INVALID_HANDLE_VALUE)
	{
	    DWORD written = 0;
	    WriteConsoleA(stdOut, res, strlen(res), &written, NULL);
	}
	Sleep(10000);
	FreeConsole();
}

void printError(char * res)
{
	AllocConsole();

	HANDLE stdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	if (stdOut != NULL && stdOut != INVALID_HANDLE_VALUE)
	{
	    DWORD written = 0;
	    WriteConsoleA(stdOut, res, strlen(res), &written, NULL);
	}
	Sleep(10000);
	FreeConsole();
	exit(EXIT_FAILURE);
}

void wstring_to_string(const std::wstring& src, std::string& dest)
{
    std::string tmp;
    tmp.resize(src.size());
    std::transform(src.begin(),src.end(),tmp.begin(),wctob);
    tmp.swap(dest);
}

std::string getParamsFile()
{
	LPWSTR *szArglist;
   	int nArgs;

   	szArglist = CommandLineToArgvW(GetCommandLineW(), &nArgs);

	std::string test = "";
	wstring_to_string((const LPWSTR)(szArglist[1]), test);
   	LocalFree(szArglist);

	HANDLE FileHandle;
	DWORD R;
	DWORD Size;
	char Line[4096] = {0};

	FileHandle = CreateFile(test.c_str(),GENERIC_READ,0,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);

	if (FileHandle == INVALID_HANDLE_VALUE)
	{
		const char *message = "Error:\nCan't find file: ";
	   	char res[255];
	   	snprintf(res, sizeof(res), "%s%s", message, test.c_str());
	   	printError(res);
	}

	Size = GetFileSize(FileHandle, &Size);
	ReadFile(FileHandle, Line, Size, &R, NULL);
	CloseHandle(FileHandle);

	std::string t(reinterpret_cast< char const* >(Line));
	test.clear();
	return t;
}

std::string getFile(std::string &test)
{
	HANDLE FileHandle;
	DWORD R;
	DWORD Size;
	char Line[4096] = {0};

	FileHandle = CreateFile(test.c_str(),GENERIC_READ,0,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);

	if (FileHandle == INVALID_HANDLE_VALUE)
	{
		const char *message = "Error:\nCan't find file: ";
	   	char res[255];
	   	snprintf(res, sizeof(res), "%s%s", message, test.c_str());
	   	printError(res);
	}

	Size = GetFileSize(FileHandle, &Size);
	ReadFile(FileHandle, Line, Size, &R, NULL);
	CloseHandle(FileHandle);

	std::string t(reinterpret_cast< char const* >(Line));
	return t;
}