#include "..\includes\Sprite.h"

Sprite::Sprite(Bitmap *pBitmap, POINT ptPosition, BOOL bCollision, int Event )
{
	mBitmap = pBitmap;
	SetRect(&mrPosition, ptPosition.x, ptPosition.y, ptPosition.x + pBitmap->getWidth(), ptPosition.y + pBitmap->getHeight());
	mBoolCol = bCollision;
	mHealth = 3;
	mEvent = Event;
	// sEnemyAnim.top = mrPosition.top + 20;
	// sEnemyAnim.bottom = mrPosition.top - 20;
	CalcCollisionRect();
}


Sprite::~Sprite()
{
}

void Sprite::Draw(HDC hdc, BOOL bTrans)
{
	if (mBitmap != NULL)
		mBitmap->Draw(hdc, mrPosition.left, mrPosition.top, bTrans);
}

void Sprite::Update(int x, int y)
{
	POINT newPos;

	newPos.x = mrPosition.left + x;
	newPos.y = mrPosition.top + y;

	setPosition(newPos);
}