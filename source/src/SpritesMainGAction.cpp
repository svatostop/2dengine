#include "..\includes\GameEngine.h"

BOOL GameEngine::updateBgFrame(int x, int y, Player *pl,  RECT &oldPos)
{
   std::vector<Background *>::iterator spriteIt;
   RECT tmpPos = pl->getPosition();
   RECT bounds = getWinBounds();

   for (spriteIt = mBground.begin(); spriteIt != mBground.end(); spriteIt++)
   {
      (*spriteIt)->Update(x, y);

      if ((*spriteIt)->getBoolCol() == TRUE)
      {
         if (pl->testCollision((*spriteIt)) || tmpPos.left < bounds.left
            || tmpPos.left > bounds.right || tmpPos.top < bounds.top
            || tmpPos.top > bounds.bottom)
         {
            pl->setPosition({oldPos.left, oldPos.top});
         }
      }
   }
}

BOOL GameEngine::updateBgFrSec(int x, int y, Player *pl,  RECT &oldPos)
{
   std::vector<Background *>::iterator spriteIt;
   RECT tmpPos = pl->getPosition();
   RECT bounds = getWinBounds();

   for (spriteIt = mSecViewBground.begin(); spriteIt != mSecViewBground.end(); spriteIt++)
   {
      (*spriteIt)->Update(x, y);

      if ((*spriteIt)->getBoolCol() == TRUE)
      {
         if (pl->testCollision((*spriteIt)) || tmpPos.left < bounds.left
            || tmpPos.left > bounds.right || tmpPos.top < bounds.top
            || tmpPos.top > bounds.bottom)
         {
            pl->setPosition({oldPos.left, oldPos.top});
         }
      }
   }
}

void GameEngine::updateEnemyAnimSpriteSec()
{
   std::vector<Sprite *>::iterator spriteIt;

   for (spriteIt = mEnemySpriteSec.begin(); spriteIt != mEnemySpriteSec.end(); spriteIt++)
   {
      RECT curP = (*spriteIt)->getPosition();
      RECT anim = (*spriteIt)->getEnemyAnim();
      RECT animSec = (*spriteIt)->getEnemyAnimSec();

      if ((*spriteIt)->getEvent() == ENEMY_PLAT)
      {
         if ((*spriteIt)->getAnimDownSec() != TRUE && curP.left >= animSec.left + 120)
            (*spriteIt)->setAnimDownSec(TRUE);
         if (curP.left <= animSec.left)
            (*spriteIt)->setAnimDownSec(FALSE);

         if ((*spriteIt)->getAnimDownSec() == TRUE && curP.left > animSec.left - 120)
         {
            (*spriteIt)->Update(-10, 0);
         }
         if ((*spriteIt)->getAnimDownSec() == FALSE && curP.left <= animSec.left + 120)
         {
             (*spriteIt)->Update(10, 0);
         }
      }

   }
}

void GameEngine::updateEnemyAnimSprite()
{
   std::vector<Sprite *>::iterator spriteIt;

   for (spriteIt = mEnemySprite.begin(); spriteIt != mEnemySprite.end(); spriteIt++)
   {
      RECT curP = (*spriteIt)->getPosition();
      RECT anim = (*spriteIt)->getEnemyAnim();
      RECT animSec = (*spriteIt)->getEnemyAnimSec();

      if ((*spriteIt)->getEvent() == ENEMY)
      {
         if ((*spriteIt)->getAnimDown() != 1 && curP.top >= anim.top - 120)
            (*spriteIt)->setAnimDown(0);
         if ((*spriteIt)->getAnimDown() != 1 && curP.top == anim.top + 120)
            (*spriteIt)->setAnimDown(2);
         if (curP.left == anim.left + 60)
            (*spriteIt)->setAnimDown(1);
         if (curP.top == anim.top && curP.left <= anim.left)
            (*spriteIt)->setAnimDown(3);



         if ((*spriteIt)->getAnimDown() == 0 && curP.top <= anim.top + 120)
         {

            (*spriteIt)->Update(-10, 10);
         }
         else if ((*spriteIt)->getAnimDown() == 1 && curP.top >= anim.top)
         {
            (*spriteIt)->Update(-10, -10);
         }
          else if ((*spriteIt)->getAnimDown() == 2 && curP.left <= anim.left + 60)
         {
            (*spriteIt)->Update(10, 0);
         }
         else if ((*spriteIt)->getAnimDown() == 3 && curP.left <= anim.left + 60)
         {
            (*spriteIt)->Update(10, 0);
         }
      }
      if ((*spriteIt)->getEvent() == ATTACK)
      {
         if ((*spriteIt)->getAnimDownSec() != TRUE && curP.left >= animSec.left + 120)
            (*spriteIt)->setAnimDownSec(TRUE);
         if (curP.left <= animSec.left)
            (*spriteIt)->setAnimDownSec(FALSE);

         if ((*spriteIt)->getAnimDownSec() == TRUE && curP.left > animSec.left - 120)
         {
            (*spriteIt)->Update(-10, 0);
         }
         if ((*spriteIt)->getAnimDownSec() == FALSE && curP.left <= animSec.left + 120)
         {
             (*spriteIt)->Update(10, 0);
         }
      }

   }
}

void GameEngine::updateSecViewSpriteEnemy(int upd)
{
   std::vector<Sprite *>::iterator spriteIt;

   for (spriteIt = mEnemySpriteSec.begin(); spriteIt != mEnemySpriteSec.end(); spriteIt++)
   {
         if ((*spriteIt)->getEvent() != HIDE && upd == 1)
         {
            if ((*spriteIt)->getEvent() == HIDE_PLAT)
            {
               RECT animPosCheck = (*spriteIt)->getEnemyAnimSec();

               (*spriteIt)->setEvent(ENEMY_PLAT);
               RECT s = (*spriteIt)->getPosition();
               s.top = animPosCheck.top;
               (*spriteIt)->setEnemyAnimSec({s.left, s.top});
               (*spriteIt)->setPosition({s.left, s.top});
               
            }
         }
         else if ((*spriteIt)->getEvent() != HIDE)
         {
            if ((*spriteIt)->getEvent() == ENEMY_PLAT)
            {
               RECT animPosCheck = (*spriteIt)->getEnemyAnimSec();

               (*spriteIt)->setEvent(HIDE_PLAT);
               RECT rc = (*spriteIt)->getEnemyAnimSec();
               //rc.top = animPosCheck.top;
               (*spriteIt)->setPosition({rc.left, rc.top});
            }
         }
   }
}


void GameEngine::updateSecViewSprite(int upd)
{
   std::vector<Sprite *>::iterator spriteIt;

   for (spriteIt = mEnemySprite.begin(); spriteIt != mEnemySprite.end(); spriteIt++)
   {
         if ((*spriteIt)->getEvent() != HIDE && upd == 1)
         {
               RECT s = (*spriteIt)->getPosition();
               (*spriteIt)->saveEnemyPos(s);
               (*spriteIt)->setPosition({s.left, 280});
               (*spriteIt)->setEvent(ATTACK);
         }
         else if ((*spriteIt)->getEvent() != HIDE)
         {
               RECT curPos = (*spriteIt)->getPosition();
               RECT rc = (*spriteIt)->getEnemyAnim();
               (*spriteIt)->setPosition({rc.left, rc.top});
               RECT te = (*spriteIt)->getPosition();
               (*spriteIt)->setEnemyAnimSec(te);
               (*spriteIt)->setEvent(ENEMY);
         }
   }
}

BOOL GameEngine::checkBounds(Player *pl, RECT &oldPos)
{
   std::vector<Background *>::iterator spriteIt;
   for (spriteIt = mBground.begin(); spriteIt != mBground.end(); spriteIt++)
   {
      if ((*spriteIt)->getBoolCol() == TRUE)
      {
         if (pl->testCollision((*spriteIt)))
         {
            pl->setPosition({oldPos.left, oldPos.top});
            return TRUE;
         }
      }
   }
   return FALSE;
}


BOOL GameEngine::checkEnemyBounds(Player *pl, RECT &oldPos)
{
   std::vector<Sprite *>::iterator spriteIt;
  
   for (spriteIt = mEnemySprite.begin(); spriteIt != mEnemySprite.end(); spriteIt++)
   {
      if ((*spriteIt)->getEvent() == ATTACK)
      {
         if (pl->testCollSprites((*spriteIt)) && (pl->getJumpStart() == TRUE))
         {
            pl->setPosition({oldPos.left - 40, oldPos.top - 40});
            (*spriteIt)->decHealth();
            if ((*spriteIt)->getHealth() == 0)
               (*spriteIt)->setEvent(HIDE);
            
            pl->setJumpStart(FALSE);

            pl->setJumpEnd(TRUE);
            return FALSE;
         }
         else if (pl->testCollSprites((*spriteIt)) && (pl->getJumpStart() == FALSE))
         {
            pl->decHealth();
            pl->setPosition({oldPos.left - 80, oldPos.top});
            if (pl->getHealth() == 0)
               GameEngine::getEngine()->setGameState(GAME_STATE_END);

            return TRUE;

         }
      }
   }
   return FALSE;
}


BOOL GameEngine::checkEnemyBoundsSec(Player *pl, RECT &oldPos)
{
   std::vector<Sprite *>::iterator spriteIt;
  
   for (spriteIt = mEnemySpriteSec.begin(); spriteIt != mEnemySpriteSec.end(); spriteIt++)
   {
      if ((*spriteIt)->getEvent() == ENEMY_PLAT)
      {
         if (pl->testCollSprites((*spriteIt)) && (pl->getJumpStart() == TRUE))
         {
            pl->setPosition({oldPos.left - 40, oldPos.top - 40});
            (*spriteIt)->decHealth();
            if ((*spriteIt)->getHealth() == 0)
               (*spriteIt)->setEvent(HIDE);
            
            pl->setJumpStart(FALSE);
            pl->setJumpEnd(TRUE);

            return FALSE;
         }
         else if (pl->testCollSprites((*spriteIt)) && (pl->getJumpStart() == FALSE))
         {
            pl->decHealth();
            pl->setPosition({oldPos.left - 80, oldPos.top});
            if (pl->getHealth() == 0)
               GameEngine::getEngine()->setGameState(GAME_STATE_END);
            return TRUE;

         }
      }
   }
   return FALSE;
}