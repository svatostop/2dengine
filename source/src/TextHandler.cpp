#include "..\includes\GameEngine.h"
#include "..\res\Resource.h"

std::map<int, std::vector<std::string>> textH;
std::map<int, std::vector<std::string>> textInit;

int lineCounter;

void initTexts()
{
	lineCounter = 0;
	textInit[0] =  {"Hello,", "it looks like you don't remember anything.", "Yesterday you fought for your state, you", "were above everyone and stronger than everyone.",
"But something happened and you can't remember what exactly it was.", "Even objects around you seem alien to you.", "First, you have to explore the area.","It's the only way to find your troops.",
"What if you've already lost and your country no longer exists?", "(Press Enter)"};
	textInit[1] = {"Level 2", "(Press Enter)"};
	
	textH[0] = {"if you still don't understand what is happening around you", "try to look from a different angle","press <E>, to go back - <F>",
	"and by the way ..", "Beware of the 'blue one'","For some reason they are especially aggressive today.."  };
	textH[1] = {"and by the way ..", "Beware of the 'blue one'","For some reason they are especially aggressive today.." };

}

void WriteTextInit(HWND hWindow, int num)
{
	HDC   hdc;
	RECT  rect;
	int height;
	hdc = GetDC(hWindow);

	Bitmap *paused = new Bitmap(hdc, 600, 400 , RGB(32,32,32));

	paused->Draw(hdc,  0 , 0);
	SetRect(&rect, 0,25, paused->getWidth(), 75);

	HFONT hfont =  CreateFont( -18, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, "GOST 2.304-81");
	SelectObject(hdc,hfont);
	SetTextColor(hdc, RGB(255, 255, 255));

	std::vector<std::string> lines = textInit[num];

	SetBkMode(hdc, TRANSPARENT);
	for (int i = 0; i < lines.size(); i++)
	{
		height = DrawText(hdc, TEXT(lines[i].c_str()), -1, &rect,
		DT_SINGLELINE | DT_CENTER | DT_VCENTER);
		OffsetRect(&rect, 0, height);
	}
	DeleteObject(hfont);
	delete paused;
	ReleaseDC(hWindow, hdc);
}

int WriteText(HWND hWindow, HINSTANCE ghInstance, int num, int Loop)
{

	HDC   hdc;
	RECT  rect;
	int height;
	std::vector<std::string> lines = textH[num];
	if (lineCounter >= lines.size())
	{
		lineCounter = 0;
		return 1;
	}

	hdc = GetDC(hWindow);

	Bitmap *paused = new Bitmap(hdc, 600, 100 , RGB(32,32,32));
	Bitmap *bmpNext = new Bitmap(hdc, IDB_NEXT, ghInstance);

	paused->Draw(hdc,  0 , 300);
	SetRect(&rect, 0,310, paused->getWidth(), 330);

	HFONT hfont =  CreateFont( -18, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, "GOST 2.304-81");
	SelectObject(hdc,hfont);
	SetTextColor(hdc, RGB(255, 255, 255));


	SetBkMode(hdc, TRANSPARENT);

	if (Loop == 1)
		lineCounter += 3;
	while (lineCounter < lines.size())
	{
		height = DrawText(hdc, TEXT(lines[lineCounter].c_str()), -1, &rect,
		DT_SINGLELINE | DT_CENTER | DT_VCENTER);
		OffsetRect(&rect, 0, height);
		lineCounter++;
		if (lineCounter % 3 == 0)
		{
			bmpNext->Draw(hdc, 500, 350, TRUE);
			lineCounter-= 3;

			DeleteObject(hfont);
			delete paused;
			delete bmpNext;
			ReleaseDC(hWindow, hdc);
			return 0;
		}
	}
	bmpNext->Draw(hdc, 500, 350, TRUE);

	DeleteObject(hfont);
	delete paused;
	delete bmpNext;
	ReleaseDC(hWindow, hdc);
	if (lineCounter < lines.size())
	{
		return 0;
	}
//lineCounter = 0;
	return 1;
}