#include "..\includes\Blizzard.h"

BOOL GameInit(HINSTANCE hInstance)
{
	gpGame = new GameEngine(hInstance, TEXT("Blizzard"), TEXT("Blizzard"), 0, 0);

	if (gpGame == NULL)
		return FALSE;

	gpGame->setFrameRate(15);

	return TRUE;
}

void GameStart(HWND hWindow)
{
	srand(GetTickCount());
}

void GameEnd()
{
	delete gpGame;
}

void GameActivate(HWND hWindow)
{
	HDC hdc;
	RECT rect;

	GetClientRect(hWindow, &rect);
	hdc = GetDC(hWindow);
	DrawText(hdc, TEXT("Here comes the game!"), -1, &rect, DT_SINGLELINE | DT_CENTER | DT_VCENTER);
	ReleaseDC(hWindow, hdc);
}

void GameDeactivate(HWND hWindow)
{
	HDC hdc;
	RECT rect;

	GetClientRect(hWindow, &rect);
	hdc = GetDC(hWindow);
	DrawText(hdc, TEXT("Here game passed"), -1, &rect, DT_SINGLELINE | DT_CENTER | DT_VCENTER);
	ReleaseDC(hWindow, hdc);
}

void GamePaint(HDC hdc)
{
	HPEN hBluePen = CreatePen(PS_SOLID, 1, RGB(0, 0, 255));
HBRUSH hPurpleBrush = CreateSolidBrush(RGB(255, 0, 255));
HGDIOBJ hBrush = SelectObject(hdc, hPurpleBrush);
HGDIOBJ hPen = SelectObject(hdc, hBluePen);
Rectangle(hdc, 16, 36, 72, 70);
Rectangle(hdc, 34, 50, 54, 70);
// *** Do some drawing here! ***
SelectObject(hdc, hPen);
SelectObject(hdc, hBrush);
DeleteObject(hPurpleBrush);
DeleteObject(hBluePen);

}

void GameCycle()
{
	HDC hdc;
	HWND hWindow = gpGame->getWindow();

	hdc = GetDC(hWindow);
	DrawIcon(hdc, rand() % gpGame->getWidth(), rand() % gpGame->getHeight(), (HICON)(WORD)GetClassLong(hWindow, GCL_HICON));
	ReleaseDC(hWindow, hdc);
}