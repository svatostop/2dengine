
#include "..\includes\CropCircles.h"

BOOL GameInit(HINSTANCE hInstance)
{
	gpGame = new GameEngine(hInstance, TEXT("Crop"), TEXT("Crop"), 0, 0);

	if (gpGame == NULL)
		return FALSE;
	gpGame->setFrameRate(15);

	return TRUE;
}

void GameStart(HWND hWindow)
{
	srand(GetTickCount());

	grcRectangle.left = gpGame->getWidth() * 2 / 5;
	grcRectangle.top = gpGame->getHeight() * 2 /5;
	grcRectangle.right = grcRectangle.left + gpGame->getWidth() / 10;
	grcRectangle.bottom = grcRectangle.right + gpGame->getWidth() / 10;
}

void GameEnd()
{
	delete gpGame;
}

void GameActivate(HWND hWindow)
{
	HDC hdc;
	RECT rect;

	GetClientRect(hWindow, &rect);
	hdc = GetDC(hWindow);
	DrawText(hdc, TEXT("Here comes the game!"), -1, &rect, DT_SINGLELINE | DT_CENTER | DT_VCENTER);
	ReleaseDC(hWindow, hdc);
}

void GameDeactivate(HWND hWindow)
{
	HDC hdc;
	RECT rect;

	GetClientRect(hWindow, &rect);
	hdc = GetDC(hWindow);
	DrawText(hdc, TEXT("Here game passed"), -1, &rect, DT_SINGLELINE | DT_CENTER | DT_VCENTER);
	ReleaseDC(hWindow, hdc);
}


void GamePaint(HDC hdc)
{
	RECT rect;
	GetClientRect(gpGame->getWindow(), &rect);
	HBRUSH hBrush = CreateSolidBrush(RGB(128, 128, 0));
	FillRect(hdc, &rect, hBrush);
	DeleteObject(hBrush);
}

void GameCycle()
{
	RECT rect;
	HDC hdc;
	HWND hWindow = gpGame->getWindow();

	int iXLast = grcRectangle.left + (grcRectangle.right - grcRectangle.left) / 2;
	int iYLast = grcRectangle.top + (grcRectangle.bottom - grcRectangle.top) / 2;

	GetClientRect(gpGame->getWindow(), &rect);
	int iInflatioon = (rand() % 17) - 16;
	InflateRect(&grcRectangle, iInflatioon, iInflatioon);
	OffsetRect(&grcRectangle, rand() % (rect.right - rect.left) - grcRectangle.left,
		 rand() % (rect.bottom - rect.top) - grcRectangle.top);

	hdc = GetDC(hWindow);
	HPEN hPen = CreatePen(PS_SOLID , 5, RGB(192, 192, 0));
	SelectObject(hdc, hPen);

	MoveToEx(hdc, iXLast, iYLast, NULL);
	LineTo(hdc, grcRectangle.left + (grcRectangle.right - grcRectangle.left) / 2,
		grcRectangle.top + (grcRectangle.bottom - grcRectangle.top) / 2);

	HBRUSH hBrush = CreateSolidBrush(RGB(192, 192, 0));
	SelectObject(hdc, hBrush);
	Ellipse(hdc, grcRectangle.left, grcRectangle.top, grcRectangle.right, grcRectangle.bottom);
	ReleaseDC(hWindow, hdc);
	DeleteObject(hBrush);
	DeleteObject(hPen);
}