#include "..\includes\Cycles.h"

BOOL GameInit(HINSTANCE hInstance)
{
	gpGame = new GameEngine(hInstance, TEXT("Cycles"), TEXT("Cycles"), IDI_SLIDESHOW, IDI_SLIDESHOW_SM, 500 , 400);
	if (gpGame == NULL)
		return FALSE;

	gpGame->setFrameRate(30);

	ghInstance = hInstance;
	return TRUE;
}

void GameStart(HWND hWindow)
{
  HDC hdc = GetDC(hWindow);
  gpBackground = new Bitmap(hdc, IDB_BACKGROUND, ghInstance);
  gpCycle[0][0] = new Bitmap(hdc, IDB_CYCLEBLUE_0, ghInstance);
  gpCycle[0][1] = new Bitmap(hdc, IDB_CYCLEBLUE_90, ghInstance);
  gpCycle[0][2] = new Bitmap(hdc, IDB_CYCLEBLUE_180, ghInstance);
  gpCycle[0][3] = new Bitmap(hdc, IDB_CYCLEBLUE_270, ghInstance);
  gpCycle[1][0] = new Bitmap(hdc, IDB_CYCLEORANGE_0, ghInstance);
  gpCycle[1][1] = new Bitmap(hdc, IDB_CYCLEORANGE_90, ghInstance);
  gpCycle[1][2] = new Bitmap(hdc, IDB_CYCLEORANGE_180, ghInstance);
  gpCycle[1][3] = new Bitmap(hdc, IDB_CYCLEORANGE_270, ghInstance);

  NewGame();
}

void GameEnd()
{
	delete gpBackground;
	for (int i = 0; i < 4; i++)
	{
		delete gpCycle[0][i];
		delete gpCycle[1][i];
	}

	delete gpGame;
}

void GameActivate(HWND hWindow)
{

}

void GameDeactivate(HWND hWindow)
{

}

void GamePaint(HDC hdc)
{
	gpBackground->Draw(hdc, 0, 0);

	for (int i = 0; i < 2; i++)
	{
		HPEN hPen = CreatePen(PS_SOLID, 5, (i==0) ? RGB(0,0,225):RGB(225, 146, 73));
		SelectObject(hdc, hPen);

		MoveToEx(hdc, gptCycleTrail[i][0].x, gptCycleTrail[i][0].y, NULL);

		for (int j= 1; i < giTrailLen[i]; j++)
		{
			LineTo(hdc, gptCycleTrail[i][j].x, gptCycleTrail[i][j].y);
		}
		DeleteObject(hPen);
	}

	int iDirecion[2] = {0, 0};
	for (int i = 0; i < 2; i++)
	{
		if (gptCycleSpeed[i].y < 0)
			iDirecion[i] = 0;
		else if (gptCycleSpeed[i].x > 0)
			iDirecion[i] = 1;
		else if (gptCycleSpeed[i].y > 0)
			iDirecion[i] = 2;
		else if (gptCycleSpeed[i].x < 0)
			iDirecion[i] = 3;
	}

	gpCycle[0][iDirecion[0]]->Draw(hdc, gptCyclePos[0].x, gptCyclePos[0].y, TRUE);
	gpCycle[1][iDirecion[1]]->Draw(hdc, gptCyclePos[1].x, gptCyclePos[1].y, TRUE);

}

void GameCycle()
{
	if (!gbGameOver)
	{
		updateCycles();

		InvalidateRect(gpGame->getWindow(), NULL, FALSE);
	}
}

void HandleKeys()
{
  if (!gbGameOver)
  {
    if (GetAsyncKeyState(VK_UP) < 0)
      steerCycle(0, 0);
    else if (GetAsyncKeyState(VK_RIGHT) < 0)
      steerCycle(0, 1);
    else if (GetAsyncKeyState(VK_DOWN) < 0)
      steerCycle(0, 2);
    else if (GetAsyncKeyState(VK_LEFT) < 0)
      steerCycle(0, 3);
  }
  else if (GetAsyncKeyState(VK_ESCAPE) < 0)
    NewGame();
}

void NewGame()
{
	gptCyclePos[0].x = 250 - (gpCycle[0][0]->getWidth() / 2);
	gptCyclePos[0].y = 400 - gpCycle[0][0]->getHeight();
	gptCycleSpeed[0].x = 0;
	gptCycleSpeed[0].y = -giSpeed;

	gptCyclePos[1].x = 250 - (gpCycle[1][0]->getWidth() / 2);
	gptCyclePos[1].y = 0;
	gptCycleSpeed[1].x = 0;
	gptCycleSpeed[1].y = giSpeed;

	giTrailLen[0] = giTrailLen[1] = 2;
	gptCycleTrail[0][0].x = gptCycleTrail[0][1].x = 250;
	gptCycleTrail[0][0].y = gptCycleTrail[0][1].y = 400;
	gptCycleTrail[1][0].x = gptCycleTrail[1][1].x = 250;
	gptCycleTrail[1][0].y = gptCycleTrail[1][1].y = 0;

	gbGameOver = FALSE;
}


void updateCycles()
{
  for (int i = 0; i < 2; i++)
  {
    // Update the light cycle position based on its speed
    gptCyclePos[i].x = gptCyclePos[i].x + gptCycleSpeed[i].x;
    gptCyclePos[i].y = gptCyclePos[i].y + gptCycleSpeed[i].y;

    // Update the light cycle trail based on its new position
    gptCycleTrail[i][giTrailLen[i] - 1].x =
      gptCyclePos[i].x + (gpCycle[i][0]->getWidth() / 2);
    gptCycleTrail[i][giTrailLen[i] - 1].y =
      gptCyclePos[i].y + (gpCycle[i][0]->getHeight() / 2);

    // See if the light cycle ran into the edge of the screen
    if (gptCyclePos[i].x < 0 ||
      gptCyclePos[i].x > (500 - gpCycle[i][0]->getWidth()) ||
      gptCyclePos[i].y < 0 ||
      gptCyclePos[i].y > (400 - gpCycle[i][0]->getHeight()))
    {
      // The game is over
      EndGame(1 - i);
      return;
    }

    // See if the light cycle collided with its own trail
    RECT rcTmpTrail;
    if (giTrailLen[i] > 2) // Must have steered at least once
    {
      for (int j = 0; j < giTrailLen[i] - 2; j++)
      {
        rcTmpTrail.left = std::min(gptCycleTrail[i][j].x, gptCycleTrail[i][j + 1].x) - 1;
        rcTmpTrail.right = std::max(gptCycleTrail[i][j].x, gptCycleTrail[i][j + 1].x) + 1;
        rcTmpTrail.top = std::min(gptCycleTrail[i][j].y, gptCycleTrail[i][j + 1].y) - 1;
        rcTmpTrail.bottom = std::max(gptCycleTrail[i][j].y, gptCycleTrail[i][j + 1].y) + 1;
        if (PtInRect(&rcTmpTrail, gptCycleTrail[i][giTrailLen[i] - 1]) != 0)
        {
          // The game is over
          EndGame(1 - i);
          return;
        }
      }
    }

    // See if the light cycle collided with the other cycle's trail
    for (int j = 0; j <= giTrailLen[1 - i] - 2; j++)
    {
      rcTmpTrail.left = std::min(gptCycleTrail[1 - i][j].x, gptCycleTrail[1 - i][j + 1].x) - 3;
      rcTmpTrail.right = std::max(gptCycleTrail[1 - i][j].x, gptCycleTrail[1 - i][j + 1].x) + 3;
      rcTmpTrail.top = std::min(gptCycleTrail[1 - i][j].y, gptCycleTrail[1 - i][j + 1].y) - 3;
      rcTmpTrail.bottom = std::max(gptCycleTrail[1 - i][j].y, gptCycleTrail[1 - i][j + 1].y) + 3;
      if (PtInRect(&rcTmpTrail, gptCycleTrail[i][giTrailLen[i] - 1]) != 0)
      {
        // The game is over
        EndGame(1 - i);
        return;
      }
    }
  }
}


void steerCycle(int iCycle, int iDir)
{
  // Remember the old light cycle speed
  POINT ptOldSpeed;
  ptOldSpeed.x = gptCycleSpeed[iCycle].x;
  ptOldSpeed.y = gptCycleSpeed[iCycle].y;

  // Change the speed of the light cycle to steer it
  switch (iDir)
  {
  case 0: // Up (0 degrees)
    if (gptCycleSpeed[iCycle].y == 0)
    {
      gptCycleSpeed[iCycle].x = 0;
      gptCycleSpeed[iCycle].y = -giSpeed;
    }
    break;

  case 1: // Right (90 degrees)
    if (gptCycleSpeed[iCycle].x == 0)
    {
      gptCycleSpeed[iCycle].x = giSpeed;
      gptCycleSpeed[iCycle].y = 0;
    }
    break;

  case 2: // Down (180 degrees)
    if (gptCycleSpeed[iCycle].y == 0)
    {
      gptCycleSpeed[iCycle].x = 0;
      gptCycleSpeed[iCycle].y = giSpeed;
    }
    break;

  case 3: // Left (270 degrees)
    if (gptCycleSpeed[iCycle].x == 0)
    {
      gptCycleSpeed[iCycle].x = -giSpeed;
      gptCycleSpeed[iCycle].y = 0;
    }
    break;
  }

  // If the speed changed, move to a new point in the light cycle trail
  if ((gptCycleSpeed[iCycle].x != ptOldSpeed.x) ||
    (gptCycleSpeed[iCycle].y != ptOldSpeed.y))
  {
    // Increment the number of trail points
    giTrailLen[iCycle]++;

    // Set the initial position of the new trail point
    gptCycleTrail[iCycle][giTrailLen[iCycle] - 1].x =
      gptCyclePos[iCycle].x + (gpCycle[iCycle][0]->getWidth() / 2);
    gptCycleTrail[iCycle][giTrailLen[iCycle] - 1].y =
      gptCyclePos[iCycle].y + (gpCycle[iCycle][0]->getHeight() / 2);
  }
}

void EndGame(int iCycle)
{
  // Set the game over flag
  gbGameOver = TRUE;

  // Display a message about the winner
  if (iCycle == 0)
    MessageBox(gpGame->getWindow(), TEXT("Blue wins!"), TEXT("Light Cycles"), MB_OK);
  else
    MessageBox(gpGame->getWindow(), TEXT("Orange wins!"), TEXT("Light Cycles"), MB_OK);
}

