#include "..\includes\Planets.h"

BOOL GameInit(HINSTANCE hInstance)
{
	gpGame = new GameEngine(hInstance, TEXT("Planets"), TEXT("Planets"), IDI_SLIDESHOW, IDI_SLIDESHOW_SM, 600, 400);
	if (gpGame == NULL)
		return FALSE;

	gpGame->setFrameRate(30);
	ghInstance = hInstance;

	return TRUE;
}

void GameStart(HWND hWindow)
{
	srand(GetTickCount());

	gOffsetScreenDC = CreateCompatibleDC(GetDC(hWindow));
	gOffsetScreenBitmap = CreateCompatibleBitmap(GetDC(hWindow), gpGame->getWidth(), gpGame->getHeight());
	SelectObject(gOffsetScreenDC, gOffsetScreenBitmap);

	HDC hdc = GetDC(hWindow);

	gpGalaxyBitmap = new Bitmap (hdc, IDB_GALAXY, ghInstance);

	player = new Bitmap (hdc, IDB_PLAYER, ghInstance);

	floor = new Bitmap(hdc, IDB_FLOOR, ghInstance);
	cell = new Bitmap (hdc, IDB_CELL, ghInstance);

	RECT rBounds = {0, 0, 600, 400};
	Background *pBG;

	
// 	std::string s = {R"(
// 11111111111111111111111111111111111111
// 1....................................1 11111111
// 1....................................1 1......1
// 1....................................111......1
// 1....@........................................1
// 1.............................................1
// 1....................................1111.....1
// 1....................................1  1111111
// 11111111111111111111111111111111111111)"};

		std::string s = {R"(
1111111111
1........1
1........1
1........1
1....@...1
1........1
1111111111
1........1
1111111111111)"};
	std::istringstream map;
	map.str(s);
std::string tmp;

int z = 0;
int l = 0;
int y = 0;

	int kof = 9 * 50;

	if (kof < 400)
		l = (400 - kof)/2;
	while (std::getline(map, tmp))
	{
		z = 0;
		for (int i = 0; tmp[i]; i++)
		{
			if (tmp[i] == '1')
			{
				pBG = new Background(cell, {z, l}, 'c');
				gpGame->addBackground(pBG);
		
			}
			if (tmp[i] == '.' || tmp[i] == '@')
			{
				if (tmp[i] == '@')
				{
					pPlayer = new Player(player, {z, l});
				}
				pBG = new Background(floor, {z, l}, 'f');
				gpGame->addBackground(pBG);
			
			}
			z += 50;
		}
		l += 50;

	}

}

void GameEnd()
{
	DeleteObject(gOffsetScreenBitmap);
	DeleteDC(gOffsetScreenDC);

	delete gpGalaxyBitmap;

	delete floor;

	delete cell;

	delete player;
	//gpGame->cleanupSprite();
	gpGame->cleanupBG();

	delete pPlayer;
	delete gpGame;
}


void GameActivate(HWND hWindow)
{
}

void GameDeactivate(HWND hWindow)
{
}

void GamePaint(HDC hdc)
{
	gpGalaxyBitmap->Draw(hdc, 0 ,0);
	
	//gpGame->drawSprite(hdc);
	gpGame->drawBackground(hdc);

	pPlayer->Draw(hdc);

}

void GameCycle()
{
	
	//InvalidateRect(gpGame->getWindow(), NULL, FALSE);

	// gpGame->updateSprite();

	HWND hWindow = gpGame->getWindow();
	HDC hdc = GetDC(hWindow);

	GamePaint(gOffsetScreenDC);

	BitBlt(hdc, 0,0, gpGame->getWidth(), gpGame->getHeight(), gOffsetScreenDC, 0,0, SRCCOPY);
	ReleaseDC(hWindow, hdc);
}


void HandleKeys()
{
	RECT oldPos;

    oldPos =  pPlayer->getPosition();

	if (GetAsyncKeyState(0x41) < 0)
    	pPlayer->Update(-10,0);
	else if (GetAsyncKeyState(0x44) < 0)
		pPlayer->Update(10,0);
	if (GetAsyncKeyState(0x53) < 0)
		pPlayer->Update(0,10);
	else if (GetAsyncKeyState(0x57) < 0)
		pPlayer->Update(0,-10);

	gpGame->checkBounds(pPlayer, oldPos);
}

// void MouseButtonDown(int x, int y, BOOL left)
// {
// 	if (left && (gDragSprite == NULL))
// 	{
// 		if ((gDragSprite = gpGame->isPointInSprite(x, y)) != NULL)
// 		{
// 			SetCapture(gpGame->getWindow());
// 			MouseMove(x,y);
// 		}
// 	}
// }

// void MouseButtonUp(int x, int y, BOOL left)
// {
// 	ReleaseCapture();

// 	gDragSprite = NULL;
// }

// void MouseMove(int x, int y)
// {
// 	if (gDragSprite != NULL)
// 	{
// 		gDragSprite->setPosition(x - (gDragSprite->getWidth()/2),
// 			y - (gDragSprite->getHeight()/2));

// 		InvalidateRect(gpGame->getWindow(), NULL, FALSE);
// 	}
// }
