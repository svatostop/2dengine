#include "..\includes\Sprite.h"

Sprite::Sprite(Bitmap *pBitmap)
{
	mBitmap = pBitmap;
	SetRect(&mrPosition, 0,0, pBitmap->getWidth(), pBitmap->getHeight());
	CalcCollisionRect();
	mpVelocity.x = mpVelocity.y = 0;
	miZorder = 0;
	SetRect(&mrBounds, 0,0,640, 480);
	mbBoundsAction = BA_STOP;
	mbHidden = FALSE;
}

Sprite::Sprite(Bitmap *pBitmap, RECT &rBounds, BOUNDSACTION bBoundsAction)
{
	int xPos = rand() % (rBounds.right - rBounds.left);
	int yPos = rand() % (rBounds.bottom - rBounds.top);

	mBitmap = pBitmap;
	SetRect(&mrPosition, xPos, yPos, xPos + pBitmap->getWidth(), yPos + pBitmap->getHeight());
	CalcCollisionRect();
	mpVelocity.x = mpVelocity.y = 0;
	miZorder = 0;
	CopyRect(&mrBounds, &rBounds);
	mbBoundsAction = bBoundsAction;
	mbHidden = FALSE;
}

Sprite::Sprite(Bitmap *pBitmap, POINT ptPosition, POINT pVelocity, int iZorder, RECT &rBounds, BOUNDSACTION bBoundsAction)
{
	mBitmap = pBitmap;
	SetRect(&mrPosition, ptPosition.x, ptPosition.y, ptPosition.x + pBitmap->getWidth(), ptPosition.y + pBitmap->getHeight());
	CalcCollisionRect();
	mpVelocity = pVelocity;
	miZorder = iZorder;
	CopyRect(&mrBounds, &rBounds);
	mbBoundsAction = bBoundsAction;
	mbHidden = FALSE;
}

Sprite::~Sprite()
{

}

SPRITEACTION Sprite::Update()
{
	POINT newPos, spriteSize, boundsSize;

	newPos.x = mrPosition.left + mpVelocity.x;
	newPos.y = mrPosition.top + mpVelocity.y;

	spriteSize.x = mrPosition.right - mrPosition.left;
	spriteSize.y = mrPosition.bottom - mrPosition.top;

	boundsSize.x = mrBounds.right - mrBounds.left;
	boundsSize.y = mrBounds.bottom - mrBounds.top;

	if (mbBoundsAction == BA_WRAP)
	{
		if ((newPos.x + spriteSize.x) < mrBounds.left)
			newPos.x = mrBounds.right;
		else if (newPos.x > mrBounds.right)
			newPos.x = mrBounds.left - spriteSize.x;
		if ((newPos.y + spriteSize.y) < mrBounds.top)
			newPos.y = mrBounds.bottom;
		else if (newPos.y > mrBounds.bottom)
			newPos.y = mrBounds.top - spriteSize.y;
	}
	else if (mbBoundsAction == BA_BOUNCE)
	{
		BOOL bounce = FALSE;
		POINT newVelocity = mpVelocity;

		if (newPos.x < mrBounds.left)
		{
			bounce = TRUE;
			newPos.x = mrBounds.left;
			newVelocity.x = -newVelocity.x;
		}
		else if ((newPos.x + spriteSize.x) > mrBounds.right);
		{
			bounce = TRUE;
			newPos.x = mrBounds.right - spriteSize.x;
			newVelocity.x = -newVelocity.x;
		}
		if (newPos.y < mrBounds.top)
		{
			bounce = TRUE;
			newPos.y = mrBounds.top;
			newVelocity.y = -newVelocity.y;
		}
		else if ((newPos.y + spriteSize.y) > mrBounds.bottom)
		{
			bounce = TRUE;
			newPos.y = mrBounds.bottom - spriteSize.y;
			newVelocity.y = -newVelocity.y;
		}
		if (bounce)
			setVelocity(newVelocity);
	}
	else if (mbBoundsAction == BA_DIE)
	{
		if ((newPos.x + spriteSize.x) < mrBounds.left || (newPos.x > mrBounds.right)
			|| (newPos.y + spriteSize.y) < mrBounds.top || (newPos.y > mrBounds.bottom))
			return SA_KILL;
	}
	else
	{
		if ((newPos.x < mrBounds.left) || newPos.x > (mrBounds.right - spriteSize.x))
		{
			newPos.x = std::max(mrBounds.left, std::min(newPos.x, mrBounds.right - spriteSize.x));
			setVelocity(0,0);
		}
		if ((newPos.y < mrBounds.top) || newPos.y > (mrBounds.bottom - spriteSize.y))
		{
			newPos.y = std::max(mrBounds.top, std::min(newPos.y, mrBounds.bottom - spriteSize.y));
			setVelocity(0,0);
		}
	}
	setPosition(newPos);

	return SA_NONE;
}

void Sprite::Draw(HDC hdc)
{
	if (mBitmap != NULL && !mbHidden)
		mBitmap->Draw(hdc, mrPosition.left, mrPosition.top, TRUE);
}
