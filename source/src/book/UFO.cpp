#include "..\includes\UFO.h"

BOOL GameInit(HINSTANCE hInstance)
{
	gpGame = new GameEngine(hInstance, TEXT("UFO"), TEXT("UFO"), IDI_SLIDESHOW, IDI_SLIDESHOW_SM, 500, 400);
	if (gpGame == NULL)
		return FALSE;

	gpGame->setFrameRate(30);
	ghInstance = hInstance;
	return TRUE;
}

void GameStart(HWND hWindow)
{
	HDC hdc = GetDC(hWindow);
	gpBackground = new Bitmap(hdc, IDB_BACKGROUND, ghInstance);
	gpSaucer = new Bitmap (hdc, IDB_SAUCER, ghInstance);
	giSaucerX = 250 - (gpSaucer->getWidth() / 2);
	giSaucerY = 250 - (gpSaucer->getHeight() / 2);
	giSpeedX = 0;
	giSpeedY = 0;
}

void GameEnd()
{
	delete gpBackground;
	delete gpSaucer;

	delete gpGame;
}

void GameActivate(HWND hWindow)
{
}

void GameDeactivate(HWND hWindow)
{
}

void GamePaint(HDC hdc)
{
	gpBackground->Draw(hdc, 0, 0);
	gpSaucer->Draw(hdc, giSaucerX, giSaucerY, TRUE);
}

void GameCycle()
{
	giSaucerX = std::min(500 - gpSaucer->getHeight(), std::max(0, giSaucerX + giSpeedX));
	giSaucerY = std::min(320, std::max(0, giSaucerY + giSpeedY));

	InvalidateRect(gpGame->getWindow(), NULL, FALSE);
}

void HandleKeys()
{
	if (GetAsyncKeyState(VK_LEFT) < 0)
		giSpeedX = std::max(-giMaxSpeed, --giSpeedX);
	else if (GetAsyncKeyState(VK_RIGHT) < 0)
		giSpeedX = std::min(giMaxSpeed, ++giSpeedX);
	if (GetAsyncKeyState(VK_UP) < 0)
		giSpeedY = std::max(-giMaxSpeed, --giSpeedY);
	else if (GetAsyncKeyState(VK_DOWN) < 0)
		giSpeedY = std::max(giMaxSpeed, ++giSpeedY);
}