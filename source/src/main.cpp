#include "..\includes\GameEngine.h"
#include "..\includes\Maps.h"

GameEngine *GameEngine::mGameEngine = NULL;
mainParams *pParams = NULL;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR szCmdLine, int iCmdShow)
{
   MSG msg;
   static int iTickTrigget = 0;
   int iTickCount;

   pParams = new mainParams();

   pParams->setFile(strMap1lvl);
   pParams->setMap();
   pParams->setMapView();

   if(GameInit(hInstance))
   {
      if (!GameEngine::getEngine()->Initialize(iCmdShow))
         return FALSE;
     
      while (TRUE)
      {
         if (GameEngine::getEngine()->getGameState() == GAME_STATE_END)
         {
            GameEnd();
            delete pParams;
            SendMessage (GameEngine::getEngine()->getWindow(), WM_CLOSE, 0, 0);
         }
         if (PeekMessage(&msg, NULL, 0, 0 , PM_REMOVE))
         {
            if (msg.message == WM_QUIT)
               break ;
            TranslateMessage(&msg);
            DispatchMessage(&msg);
         }
         else
         {
            if (!GameEngine::getEngine()->getSleep())
            {
               iTickCount = GetTickCount();
               if (iTickCount > iTickTrigget)
               {
                  iTickTrigget = iTickCount + GameEngine::getEngine()->getFrameDelay();
                  if (GameEngine::getEngine()->getGameState() == GAME_STATE_INIT
                  || GameEngine::getEngine()->getGameState() == GAME_STATE_INIT_NXTLVL)
                     Sleep(100);
                  GameMain();
              }
            }
         }
      }
      return (int)msg.wParam;
   }

   GameEnd();
   delete pParams;

   return TRUE;
}

LRESULT CALLBACK WndProc (HWND hWindow, UINT msg, WPARAM wParam, LPARAM lParam)
{
   return GameEngine::getEngine()->HandleEvent(hWindow, msg, wParam, lParam);
}

GameEngine::GameEngine (HINSTANCE hInstance, LPTSTR szWindowsClass, LPTSTR szTitle, WORD wIcon, WORD wSmallIcon, int iWidth, int iHeigh)
{

   mGameEngine = this;
   mhInstance = hInstance;
   mhWindow = NULL;
   if (lstrlen(szWindowsClass) > 0)
      lstrcpy(mszWindowsClass, szWindowsClass);
   if (lstrlen(szTitle) > 0)
      lstrcpy(mszTitle, szTitle);
   mwIcon = wIcon;
   mwSmallIcon = wSmallIcon;
   miWidth =  600;
   miHeght =  400;
   mSecView = pParams->getMapView();
   RECT backRect = {0,0, miWidth, miHeght};
   miFrameDelay = 50;
   mbSleep = FALSE;
   bGameViewDef = TRUE;
   mBoolKeyJmp = FALSE;
   mGameState = GAME_STATE_INTRO;
   mTextCount = 0;
   Lvlcounter = 0;
   mBground.reserve(100);
   mEnemySprite.reserve(100);
   mEnemySpriteSec.reserve(50);
   mSecViewBground.reserve(100);
   mPlatforms.reserve(50);
   mSprites.reserve(50);
   mSpritesFirst.reserve(50);
   mWindowBounds = {100, 100, getWidth()/2, getHeight() - 150};
}

GameEngine::~GameEngine()
{

}

void GameEngine::nextLevel()
{
   delete pParams;
   std::string map;
   Lvlcounter++;

   if (Lvlcounter == 1)
      map = strMap2lvl;
   if (Lvlcounter == 2)
      map = strMap3lvl;

   pParams = new mainParams();

   pParams->setFile(map);
   pParams->setMap();
   pParams->setMapView();
   mSecView = pParams->getMapView();

   mBground.reserve(100);
   mEnemySprite.reserve(100);
   mEnemySpriteSec.reserve(50);
   mSecViewBground.reserve(100);
   mPlatforms.reserve(50);
   mSprites.reserve(100);
   mSpritesFirst.reserve(50);
   mWindowBounds = {100, 100, getWidth()/2, getHeight() - 150};
}

BOOL GameEngine::Initialize(int iCmdShow)
{
     WNDCLASSEX wndclass;

   wndclass.cbSize = sizeof(wndclass);
   wndclass.style = CS_HREDRAW | CS_VREDRAW;
   wndclass.lpfnWndProc = WndProc;
   wndclass.cbClsExtra = 0;
   wndclass.cbWndExtra = 0;
   wndclass.hInstance = mhInstance;
   wndclass.hIcon = LoadIcon(mhInstance, MAKEINTRESOURCE(getIcon()));
   wndclass.hIconSm = LoadIcon(mhInstance, MAKEINTRESOURCE(getSmallIcon()));
   wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
   wndclass.lpszMenuName = NULL;
   wndclass.lpszClassName = mszWindowsClass;

   if (!RegisterClassEx(&wndclass))
      return FALSE;

   int iWindowWidth = miWidth + GetSystemMetrics(SM_CXFIXEDFRAME) * 2,
         iWindowHeight = miHeght + GetSystemMetrics(SM_CYFIXEDFRAME) * 2 +
         GetSystemMetrics(SM_CYCAPTION);
   if (wndclass.lpszMenuName != NULL)
      iWindowHeight += GetSystemMetrics(SM_CYMENU);
   int iXWindowPos = (GetSystemMetrics(SM_CXSCREEN) - iWindowWidth) / 2,
         iYWindowPos = (GetSystemMetrics(SM_CYSCREEN) - iWindowHeight) / 2;

   mhWindow = CreateWindow(mszWindowsClass, mszTitle, WS_POPUPWINDOW | WS_CAPTION | WS_MINIMIZEBOX, iXWindowPos, iYWindowPos, iWindowWidth, iWindowHeight, NULL, NULL, mhInstance, NULL);
   if (!mhWindow)
      return FALSE;


   ShowWindow(mhWindow, iCmdShow);
   UpdateWindow(mhWindow);

   return TRUE;

}

LRESULT GameEngine::HandleEvent(HWND hWindow, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch (msg)
   {
      case WM_CREATE :
         setWindow(hWindow);
         GameIntroInit(hWindow, mhInstance);
         GameStart(hWindow);
         return 0;
      case WM_SETFOCUS:
         GameActivate(hWindow);
         setSleep(FALSE);
         return 0;
      case WM_KILLFOCUS:
         GameDeactivate(hWindow);
         setSleep(TRUE);
         return 0;
      case WM_PAINT:
         HDC hdc;
         PAINTSTRUCT ps;
         hdc = BeginPaint(hWindow, &ps);

         GamePaint(hdc);

         EndPaint(hWindow, &ps);
         return 0;
      case WM_DESTROY:
         GameEnd();
         delete pParams;
         PostQuitMessage(0);
         return 0;
   }
   return DefWindowProc(hWindow, msg, wParam, lParam);
}

std::string GameEngine::getMap()
{
   return pParams->getMapStr();

}

int GameEngine::getYMapLen()
{
   return pParams->getYLen();
}
