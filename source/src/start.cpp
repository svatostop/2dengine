#include <iostream>
#include <fstream>
#include <windows.h>
#include <conio.h>

const char *help = R"V0G0N(
1. crtLvl [name]
			- create file [name] 

REQUIRED FIELDS:

RES: [W] [H]
			- [>= 300 && <= 1280] [>= 300 && <= 1024]

MAP: [mode]
1111111111111111111111111111111 
1                     3       1		
1                             1 111111
1   @                         111    1
1             3                   L  1
1                       2     111    1
1111111111111111111111111111111 111111

			-	[mode] -- mode: [1] top view or [2] profile view;
Min. level size = 10х6 
			- '1' -- level boundary
			- '@' -- character position
			- 'L' -- point of the next level 
			- '2','3',... -- sprite position

OPTIONAL FIELDS:

PLAYER: [C] [option] || [B] [option]
			- [C] [option] -- player 'box' color R,G,B
			- [B] [option] -- BMP-sprite path (must be in 'BMP'-dir)

LEVELS: [B] [option] [pathToNextLevel] || [-] [pathToNextLevel]
			- [B] [option] [pathToNextLevel] -- BMP-sprite path (must be in 'BMP'-dir) ;
	path to the next level-file (must be in 'levels'-dir)
			- [-] [pathToNextLevel] -- without sprite;
	path to the next level-file (must be in 'levels'-dir)

SPRITES: [numOfSprite] [option] 
			- [>= 2] -- [ BMP-sprite path (must be in 'BMP'-dir) ]

EVENTS: [withSprites] || [withLevel] || [withPerson]

F_COLOR: [C] [option] || [B] [option]
			- [C] [option] -- floor color R,G,B
			- [B] [option] -- BMP-sprite path (must be in 'BMP'-dir)

BG_COLOR: [C] [option] || [B] [option]
			- [C] [option] -- background color R,G,B
			- [B] [option] -- BMP-sprite path (must be in 'BMP'-dir)

2. start [levels\name]
			- [levels\name] -- path to the level-file
3. exit

)V0G0N";


void startup(std::string &exe)
{
	STARTUPINFO sti = {0};
	PROCESS_INFORMATION pi = {0};

	std::wstring CommandLine(exe.begin(), exe.end());

	LPWSTR lpwCmdLine = &CommandLine[0];

	if (!CreateProcess(NULL, lpwCmdLine, NULL, NULL, TRUE, 0, NULL, NULL, &sti, &pi))
	{
		std::cout << "Unable to create process :(" << std::endl;
		return ;
	}
}

void crtLvl()
{
	std::string levelName;
	std::string path = "engine\\levels\\"; 

	std::cin >> levelName;

	path += levelName;
	std::ofstream outfile (path);

	outfile << "RES: \n\nMAP: \n\nPLAYER: \n\nLEVELS: \n\nSPRITES: \n\nEVENTS: \n\nF_COLOR: \n\nBG_COLOR: " << std::endl;

	outfile.close();

	std::string exe = "notepad.exe " + path;

	startup(exe);
}

int main()
{
	std::cout << "\ntype \"help\" for cmd list\n";

	std::string cmd;

	while (true)
	{
		std::cout << "$CMD> " ;

		std::cin >> cmd;

		if (cmd == "help")
		{
			std::cout << help;
		}
		if (cmd == "crtLvl")
		{
			crtLvl();
		}
		if (cmd == "start")
		{
			std::string make = "cmd /c cd engine && ..\\make.exe";
			std::string path = "engine\\levels\\"; 
			std::string fileLevel;

			std::cin >> fileLevel;

			path  += fileLevel;
			std::string exe = "engine\\engine1.exe " + path;

			startup(make);

			std::cout << "\nPRESS SOME KEY!\n";
			_getch();

			startup(exe);

			return 0;
		}
		if (cmd == "exit")
		{
			return 0;
		}
		else
		{
			std::cout << "\ntype \"help\" for cmd list\n";
		}
	}
	return 0;
}